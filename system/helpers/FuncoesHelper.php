<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Ariza
 * Date: 05/11/2016
 * Time: 10:20
 */

/**
 * @param $array
 * @param bool $exit
 */
function show_array($array, $exit = false)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    if ($exit) {
        exit;
    }
}

function imprimir_xml($xml)
{
    Header('content-type: text/xml');
    echo $xml;
    exit;
}

function strpos_array($haystacks, $needle, $offset = 0)
{
    if (is_array($haystacks)) {
        foreach ($haystacks as $haystack) {
            if (strpos($haystack, $needle) === false) {
                return false;
            }
        }
        return true;
    } else {
        return strpos($haystacks, $needle, $offset);
    }
}

/**
 * @param null $postagem
 * @return mixed
 */
function input_post($postagem = NULL)
{
    if ($postagem) {
        if (isset($_POST[$postagem]) && is_array($_POST[$postagem])) {
            $post = filter_input(INPUT_POST, $postagem, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        } else {
            $post = filter_input(INPUT_POST, $postagem);
        }
    } else {
        $post = $_POST;
    }
    return $post;
}

function input($item = null)
{
    $content = file_get_contents('php://input');
    if (\Aplicacao\Ferramentas\Identificadores::eJson($content)) {
        $inputArray = json_decode($content, true);
    } else {
        $query = urldecode($content);
        parse_str($query, $inputArray);
        $inputArray = (array)$inputArray;
    }

    if ($item && isset($inputArray[$item])) {
        return $inputArray[$item];
    }
    if (($item && !isset($inputArray[$item])) && !input_post($item)) {
        return false;
    }
    if (!$inputArray) {
        return input_post($item);
    }
    return $inputArray;
}

function input_json($item = null)
{
    $data = file_get_contents('php://input');
    $data = json_decode($data, true);
    $return = (isset($data) ? $data : false);
    if (!$item) {
        return $return;
    }
    if (isset($data[$item])) {
        return $data[$item];
    }
    return $return;
}

/**
 *
 */
function input_angular_http($position = null)
{
    $httpAngular = json_decode(file_get_contents("php://input"));
    if (!$position) {
        return $httpAngular;
    }
    if (isset($httpAngular->{$position})) {
        return $httpAngular->{$position};
    }
    return false;
}

/**
 * @param null $get
 * @return mixed|null
 */
function input_get($getter = NULL, $teste = false)
{
    $get = $_GET;
    if (isset($_GET['url'])) {
        unset($_GET['url']);
    }
    if ($getter && !isset($_GET[$getter])) {
        return false;
    }
    if ($getter) {
        if (isset($_GET[$getter]) && is_array($_GET[$getter])) {
            $get = filter_input(INPUT_GET, $getter, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        } elseif (isset($_GET[$getter]) && !is_array($_GET[$getter])) {
            $get = filter_input(INPUT_GET, $getter);
            if (!$get) {
                $get = $_GET[$getter];
            }
        }
    }

    return $get;
}

function client_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip == '::1' ? '186.214.150.60' : $ip;
}

/**
 * @param null $sessao
 * @return bool|null
 */
function sessao($sessao = null)
{
    if (!$sessao) {
        return $_SESSION;
    }
    if (isset($_SESSION[$sessao])) {
        return $_SESSION[$sessao];
    }
    return false;
}


function traduzirMes($mes, $minificar = false)
{
    if (is_numeric($mes)) {
        $mes = date('F', strtotime(date('Y') . '-' . $mes . '-01'));
    }
    $mesesIngles = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
        'October', 'November', 'December');
    $mesesPortugues = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro'
    , 'Outubro', 'Novembro', 'Dezembro');
    $return = str_replace($mesesIngles, $mesesPortugues, $mes);
    if ($minificar) {
        return minificarMesPortugues($return);
    }
    return $return;
}

function minificarMesPortugues($mes)
{
    $mesesPortugues = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro'
    , 'Outubro', 'Novembro', 'Dezembro');
    $mesesIngles = array('JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET',
        'OUT', 'NOV', 'DEZ');
    return str_replace($mesesPortugues, $mesesIngles, $mes);
}

/**
 * @param $texto
 * @param int $tmanahoMaximoLink
 * @return mixed|string
 */
function criarLinksEmTexto($texto, $tmanahoMaximoLink = 800)
{
    if (!is_string($texto)) {
        return '';
    }
    preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $texto, $matchs);
    foreach ($matchs[0] as $link) {
        if (strstr($link, "http://") === false && strstr($link, "https://") === false) {
            $link = "http://" . $link;
        }
        $link_len = strlen($link);
        //troca "&" por "&", tornando o link válido pela W3C
        $web_link = str_replace("&", "&", $link);
        $texto = str_ireplace($link, "<a href=\"" . $web_link . "\" target=\"_blank\">" . (($link_len > $tmanahoMaximoLink) ? substr($web_link, 0, 25) . "..." . substr($web_link, -15) : $link) . "</a>", $texto);
    }
    return $texto;
}

/**
 * @param $texto
 * @return array
 */
function capturarLinksEmTexto($texto)
{
    if (!is_string($texto)) {
        return [];
    }
    preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $texto, $matchs);
    return isset($matchs[0]) && !empty($matchs[0]) ? $matchs[0] : [];
}

/**
 * @param $url
 * @return string
 */
function getYoutubeId($url)
{
    parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
    return isset($my_array_of_vars['v']) ? $my_array_of_vars['v'] : 0;
}

function curl($url, $method, $data)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    switch ($method) {
        case "GET":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "POST":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            break;
    }
    $response = curl_exec($curl);
    $data = json_decode($response);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    // Check the HTTP Status code
    $error_status = '';
    switch ($httpCode) {
        case 200:
            $error_status = "200: Success";
            break;
        case 404:
            $error_status = "404: API Not found";
            break;
        case 500:
            $error_status = "500: servers replied with an error.";
            break;
        case 502:
            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
            break;
        case 503:
            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
            break;
        default:
            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
            break;
    }
    return array('data_json' => $data, 'response_original' => $response, 'http_code' => $httpCode, 'error_status' => $error_status);
}

function soNumero($str)
{
    return preg_replace("/[^0-9]/", "", $str);
}