<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 10/09/2017
 * Time: 16:37
 */

namespace Api\ApiHelp\Models\Verificacoes;


use Api\ApiExeption;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Ferramentas\ResponseCode;

class Header
{

    public function verificarCampos($camposHeader = array())
    {
        try{
        foreach ($camposHeader as $campo){
            if(!Ajax::getHeader($campo)){
                throw new ApiExeption('Validacao de header requer o campo '.$campo, 1, ResponseCode::HTTP_BAD_GATEWAY);
            }
        }
        }catch (ApiExeption $e) {
            $json = array(
                'msg' => $e->getMessage(),
                'acao' => $e->getAction(),
                'erro' => true
            );
            Response::json($json, $e->getCode());
        }
        return;
    }

}