<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 01:59
 */

namespace Api\ApiHelp\Models\Verificacoes;


use Api\ApiExeption;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\ResponseCode;

class CamposObrigatorios
{

    public function verificar($camposObrigatorios = array())
    {
        $camposEnviadosPelaRequisao = input();
        if (Ajax::getRequestTipo() == 'GET') {
            $camposEnviadosPelaRequisao = input_get();
        }
        try {
            foreach ($camposObrigatorios as $campoObrigatorio => $tipoDoCampo) {
                $csmpoDaValidacaoEnviado = false;
                foreach ($camposEnviadosPelaRequisao as $campoDaRequisicao => $valorDoCampo) {
                    if ($campoDaRequisicao == $campoObrigatorio) {
                        $csmpoDaValidacaoEnviado = true;
                    }
                }
                if (!$csmpoDaValidacaoEnviado) {
                    throw new ApiExeption('Campo obrigatorio ' . $campoObrigatorio . ' esta faltando em sua requisicao!', 1, ResponseCode::HTTP_NOT_FOUND);
                }
            }
        } catch (ApiExeption $e) {
            $json = array(
                'msg' => $e->getMessage(),
                'acao' => $e->getAction(),
                'erro' => true
            );
            return \Aplicacao\Ferramentas\Response::json($json, $e->getCode());
        }

        return true;
    }

}