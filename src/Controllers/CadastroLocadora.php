<?php
/**
 * Created by PhpStorm.
 * User: Shared
 * Date: 30/04/2018
 * Time: 17:50
 */

namespace Controllers;


use Core\Controlador\Controlador;
use Core\Controlador\ControladorAdmin;

class CadastroLocadora extends ControladorAdmin
{

    public function cadastrar()
    {
        $data['pagina'] = 'CadastroLocadora/index';
        $data['titulo'] = 'teste';
        $data['descricao'] = 'Testando';
        $data['palavras_chaves'] = 'palavra1, palavra2';
        $this->load->view('Admin/index', $data);
    }
}