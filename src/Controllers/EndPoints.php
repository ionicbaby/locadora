<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 16/03/2018
 * Time: 15:37
 */

namespace Controllers;

use Api\ApiHelp\Models\Verificacoes\Header;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Url;
use Models\EndPoints\buscarCategoriaVeiculo;
use Models\EndPoints\BuscarDadosTipoVeiculo;
use Models\EndPoints\CidadesPorEstado;
use Models\EndPoints\Cron;
use Models\EndPoints\SalvarDadosFormulario;
use Models\EndPoints\Login;

class EndPoints
{

    public function buscarDadosTipoVeiculo()
    {
        $result = (new BuscarDadosTipoVeiculo())->get();
        Response::json($result, Ajax::getResponseCode());
    }

    public function buscarCategoriaVeiculo()
    {
        $resultado = (new buscarCategoriaVeiculo())->get();
        Response::json($resultado, Ajax::getResponseCode());

    }

    public function getCidadesPorEstadoId()
    {
        $resultado = (new CidadesPorEstado())->get();
        Response::json($resultado, Ajax::getResponseCode());
    }

    public function enviarFormulario()
    {
        $resultado = (new SalvarDadosFormulario())->post();
        Response::json($resultado, Ajax::getResponseCode());
    }

    public function cron()
    {
        $resultado = (new Cron())->executar();
        Response::json($resultado, Ajax::getResponseCode());
    }

    public function login()
    {
        $resultado = (new Login())->get();
        Response::json($resultado, Ajax::getResponseCode());
    }

    public function logout()
    {
        if (isset($_SESSION['admin'])) {
            unset($_SESSION['admin']);
        }
        Header('Location:' . Url::Base());
    }

    public function graficos()
    {
        $resultado = (new Login())->get();
        Response::json($resultado, Ajax::getResponseCode());
    }

}