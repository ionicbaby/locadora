<?php
/**
 * Created by PhpStorm.
 * User: Shared
 * Date: 30/04/2018
 * Time: 21:02
 */

namespace Controllers;


use Core\Controlador\ControladorAdmin;
use Models\EndPoints\GetValoresRelatorio;

class Admin extends ControladorAdmin
{
    public function index()
    {
        $getValoresRelatoriosClasse = (new GetValoresRelatorio());

        $last_week = $getValoresRelatoriosClasse->getTopDezEmailsEnviados();

        $total = $getValoresRelatoriosClasse->getTotalEmailsEnviados();
        $locadoras = $getValoresRelatoriosClasse->getTotalCadastroLocadoras();
        $total_clientes = $getValoresRelatoriosClasse->getTotalClientesAtendidos();

        $data['last_week'] = $last_week;
        $data['total_clientes'] = $total_clientes['totalClientes'];
        $data['locadoras'] = $locadoras['locadoras'];
        $data['total'] = $total['dados'];
        $data['pagina'] = 'Admin/corpo';
        $this->load->view('Admin/index', $data);
    }

    public function relatorios()
    {
        $data['pagina'] = 'Admin/Relatorios/index';
        $this->load->view('Admin/index', $data);
    }

    public function RelatoriosEmailsEnviados()
    {
        $data['pagina'] = 'Admin/Relatorios/EmailsEnviados/index';
        $this->load->view('Admin/index', $data);
    }

    public function RelatoriosCadastrosLocadoras()
    {
        $data['pagina'] = 'Admin/Relatorios/CadastroLocadoras/index';
        $this->load->view('Admin/index', $data);
    }
}