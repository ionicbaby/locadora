<?php

/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 10:42
 */

namespace Controllers;

use Core\Controlador\Controlador;

class IndexControlador extends Controlador
{

    private $clumnName = 'testing';

    /**
     * @description instanciado assim que executado
     */
    public function index()
    {
        $data['categorias'] = $this->db->order_by('order', 'ASC')->get('categorias')->result(false);
        $data['estados'] = $this->db->order_by('sigla_uf', 'ASC')->get('sys_estados')->result(false);
        $data['pagina'] = 'Home/corpo';
        $this->load->view('Home/index', $data);
    }


}