function buscarDadosTipoVeiculo($dados) {
    var url = base_url('EndPoints/buscarDadosTipoVeiculo');
    $.ajax({
        url: url,
        data: $dados,
        headers: {"token": 'free'},
        type: "GET",
        success: function (response) {

            if (typeof response.dados === 'undefined') {
                alert('Dados da categoria nao foram encontrados');
                return;
            }
            var data = response.dados;


            $('.card-body').slideDown();
            $('#n_carros').html(response.dados.n_carros);
        },
        error: function (response) {
            if (typeof response.responseJSON !== 'undefined') {

            } else {
                alert('Ocorreu um erro');
            }
        }
    });
}

function categoriasChange() {
    $('#categorias_veiculo').change(function () {
        var imagem = $('option:selected', this).attr('data-imagem');
        var titulo = $('option:selected', this).text();
        $('#conteudo_categoria').css({'opacity': '0.4'});

        $('#titulo_categoria').html(titulo);
        $('#imagem_da_categoria').attr('src', imagem);
        var dados = {categoria_id: $(this).val()};
        buscarCategoriaVeiculo(dados);
    });
}

function carregarCategoriaDefault() {
    $('#area_de_escolha_das_categorias').empty().append('Carregando...');
    setTimeout(function () {
        $('#area_de_escolha_das_categorias').empty();
        $('#categorias_veiculo').trigger('change');
    }, 500);
}

function carregarCidadesPorEstadoId($idEstado) {
    var selectCidades = $('select[name="cidades"]');
    selectCidades.attr('disabled', 'disabled');

    selectCidades.append(
        $('<option>', {
            value: '-1',
            text: 'Carregando cidades...'
        })
    );


    var url = base_url('EndPoints/getCidadesPorEstadoId');
    $.ajax({
        url: url,
        data: {sigla_estado: $idEstado},
        type: "GET",
        success: function (response) {
            if (typeof response.dados === 'undefined') {
                alert('Dados da categoria nao foram encontrados');
                return;
            }
            var data = response.dados;
            if (typeof data[0] === "undefined") {
                selectCidades.empty().append(
                    $('<option>', {
                        value: '-1',
                        text: 'Ainda não atendemos este estado.'
                    })
                );
                return;
            }
            selectCidades.empty();
            $.each(data, function (key, val) {
                selectCidades.append(
                    $('<option>', {
                        value: val.codigo,
                        text: val.titulo
                    })
                );
            });
            selectCidades.removeAttr('disabled');


        },
        error: function () {

        }
    });
}

function selecaoEstadosCidades() {
    $('select[name="estados"]').on('change', function () {
        var idEstado = $('option:selected', this).val();
        carregarCidadesPorEstadoId(idEstado);
    });
}

function login() {
    $('#login').submit(function () {
        var dados = $(this).serializeArray();
        var url = base_url('EndPoints/login');
        var logarButton = $('#logarButton');
        var buttonText = logarButton.html();
        console.log(dados);
        $.ajax({
            url: url,
            data: dados,
            type: 'GET',

            success: function (resposta) {
                logarButton.html(resposta.msg);
                logarButton.removeClass('btn-primary');
                logarButton.addClass('btn-success');
                setTimeout(function () {
                    logarButton.addClass('btn-primary');
                    logarButton.removeClass('btn-success');
                    logarButton.html(buttonText);
                    window.location = base_url('admin');
                },3000);
            },
            error: function (resposta) {
                logarButton.html(resposta.responseJSON.msg);
                logarButton.removeClass('btn-primary');
                logarButton.addClass('btn-danger');
                setTimeout(function () {
                    logarButton.addClass('btn-primary');
                    logarButton.removeClass('btn-danger');
                    logarButton.html(buttonText);
                },3000);
            }
        });
        return false;
    })
}

function enviarFormulario() {
    $('#pedir_orcamento').submit(function () {
        var dados = $(this).serializeArray();
        var url = base_url('EndPoints/enviarFormulario');
        var buttonText = $('#submitForm').html();
        $('#submitForm').html('Processando seu pedido! Aguarde...');
        $.ajax({
            url: url,
            data: dados,
            type: 'POST',
            success: function (resposta) {
                $('#submitForm').html(resposta.dados.msg);
                setTimeout(function () {
                    $('#submitForm').html(buttonText);
                }, 3000);
            },
            error: function (resposta) {
                $('#submitForm').html('Pedido não pode ser enviado! Tente novamente mais tarde');
                setTimeout(function () {
                    $('#submitForm').html(buttonText);
                }, 3000);

                console.log(resposta);

            }
        });
        return false;
    });
}


$(function () {
    categoriasChange();
    carregarCategoriaDefault();
    selecaoEstadosCidades();
    enviarFormulario();
    login();
});


function buscarCategoriaVeiculo($dados) {

    $('#area_de_escolha_das_categorias').empty().append('Carregando...');
    var url = base_url('EndPoints/buscarCategoriaVeiculo');
    $.ajax({
        url: url,
        data: $dados,
        type: 'GET',

        success: function (resposta) {
            $('#area_de_escolha_das_categorias').empty();
            if (typeof resposta.dados === 'undefined') {
                alert("Dados não encontrados");
                return;
            }
            var data = resposta.dados;
            var html = '';
            $.each(data, function (key, val) {
                html += '<span class="badge badge-primary" style="padding: 5px" >' + val.descricao + '</span> &nbsp;';
            });
            $('#dados_da_categoria').fadeIn();
            $('#atributos_da_categoria').html(html);
            setTimeout(function () {
                $('#atributos_da_categoria').html(html);
                $('#conteudo_categoria').css({'opacity': '1'});
            }, 500);
        },
        error: function () {
            $('#area_de_escolha_das_categorias').empty();
        }
    });

}