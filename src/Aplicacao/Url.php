<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/11/2016
 * Time: 13:09
 */

namespace Aplicacao;


class Url
{
    /**
     * @param null $uris
     * @return string
     */
    public static function Base($uris = NULL)
    {
        $url = LI_BASE_URL . '/' . $uris;
        return $url;
    }

    /**
     * @param null $uris
     * @return string
     */
    public static function Admin($uris = NULL)
    {
        $url = $uris ? LI_BASE_ADMIN_URL . '/' . $uris : LI_BASE_ADMIN_URL . '/';
        return $url;
    }

    /**
     * @param null $uris
     * @return string
     */
    public static function ModuloAdmin($uris = NULL)
    {
        $url = $uris ? LI_BASE_ADMIN_URL . '/Modulo/' . $uris : LI_BASE_ADMIN_URL;
        return $url;
    }

    /**
     * @description Retorna o modulo atual de acordo com a url
     * @param $url
     * @return string
     */
    public static function ModuloAtual($url)
    {
        $segmentos = (self::Segmento(0) == 'Admin' && self::Segmento(1) == 'Modulo') ||
        (self::Segmento(0) == 'admin' && self::Segmento(1) == 'Modulo') ? 3 :
            (self::Segmento(0) ? 2 : false);
        if (!$segmentos) {
            die('<code>Modulo-nao-detectado</code>');
        }
        $urlModulo = '';
        for ($i = 0; $i <= 3; $i++) {
            $segmento = self::Segmento($i);
            if ($i <= 1 && $segmento == 'Modulo') {
                $segmento = 'Modulo';
            }
            $urlModulo .= '/' . $segmento;
        }
        return Url::Base(substr($urlModulo, 1) . '/' . $url);
    }

    /**
     * @description Retorna o modulo atual de acordo com a url
     * @param $url
     * @return string
     */
    public static function getModuloAtual($url)
    {
        return self::ModuloAtual($url);
    }

    /**
     * @return string
     */
    public static function Current()
    {
        $protocolo = LI_PROTOCOLO;
        $pageURL = $protocolo . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        return $pageURL;
    }

    /**
     * @param $int
     * @return array|null|string
     */
    public static function Segmento($int = false)
    {
        $segmentos = explode('/', $_SERVER['QUERY_STRING']);
        if ($int === false) {
            return $segmentos;
        }
        if (isset($segmentos[$int])) {
            $segmento = $segmentos[$int];
        } else {
            $segmento = NULL;
        }
        return $segmento;
    }

    /**
     * @param $url
     */
    public static function Redirecionar($url)
    {
        header('Location:' . $url);
    }

    /**
     * @param null $string
     * @return mixed
     */
    public static function Title($string = NULL)
    {
        $procurar = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í',
            'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á',
            'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô',
            'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', '}', ']', '°', '+', '(', ')', '*', '#', '@', '!', '#', '$', '%', '¨', ':', '’', '‘', ',', '.', ':', 'º');
        $substituir = array('a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i',
            'i', 'i', 'd', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 's', 'a', 'a',
            'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o',
            'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', '', '', '', '', '', '', '', '', '', '', ' ', ' ', ' ', ' ', '', '', '', ' ', '', '','');
        $replace = str_replace($procurar, $substituir, $string);
        $replace = str_replace(' ', '-', $replace);
        $replace = str_replace(array('-----', '----', '---', '--'), '-', $replace);
        return strtolower($replace);
    }

    /**
     * @param $url
     * @param $newUrl
     * @return mixed
     */
    public static function Apelido($url, $newUrl)
    {
        $newUrl = self::Title($newUrl);
        $urlApelidoJson = array();
        if (!is_file('url_apelido.json')) {
            $urlApelidoJson[$newUrl]['url_apelido'] = $newUrl;
            $urlApelidoJson[$newUrl]['url_real'] = $url;
            $newUrlApelidoJson = json_encode($urlApelidoJson);
            file_put_contents('url_apelido.json', $newUrlApelidoJson);
            return $newUrl;
        }
        $urlApelidoJson = (array)json_decode(file_get_contents(Url::Base('url_apelido.json')));
        if (!is_array($urlApelidoJson)) {
            return $url;
        }
        if (isset($urlApelidoJson[$newUrl])) {
            if ($urlApelidoJson[$newUrl]->url_real != $url) {
                $urlApelidoJson[$newUrl]->url_apelido = $newUrl;
                $urlApelidoJson[$newUrl]->url_real = $url;
                file_put_contents('url_apelido.json', json_encode($urlApelidoJson));
            }
            return $newUrl;
        } else {
            $urlApelidoJson[$newUrl]['url_apelido'] = $newUrl;
            $urlApelidoJson[$newUrl]['url_real'] = $url;
            $newUrlApelidoJson = json_encode($urlApelidoJson);
            file_put_contents('url_apelido.json', $newUrlApelidoJson);
            return $newUrl;
        }
    }

    /**
     * @param bool $urlApelido
     * @return bool
     */
    public static function GetUrlRealPorUrlApelido($urlApelido = false)
    {
        if (!$urlApelido) {
            $urlApelido = $_SERVER['QUERY_STRING'];
        }
        if (!is_file('url_apelido.json')) {
            return $urlApelido;
        }
        $urlApelidoJson = (array)json_decode(file_get_contents(Url::Base('url_apelido.json')));
        if (!is_array($urlApelidoJson)) {
            return $urlApelido;
        }
        if (isset($urlApelidoJson[$urlApelido])) {
            $limitUrlApelido = 1500;
            if (count($urlApelidoJson) >= $limitUrlApelido) {
                $contador = 0;
                $novoDocumentoUrlApelido = array();
                foreach ($urlApelidoJson as $key => $value) {
                    if ($contador >= $limitUrlApelido) {
                        break;
                    }
                    $novoDocumentoUrlApelido[$key]['url_real'] = $value->url_real;
                    $novoDocumentoUrlApelido[$key]['url_apelido'] = $value->url_apelido;
                    $contador++;
                }
                file_put_contents('url_apelido.json', json_encode($novoDocumentoUrlApelido));
            }
            return $urlApelidoJson[$urlApelido]->url_real;
        }
        return $urlApelido;
    }

    /**
     * @return string
     */
    public static function VoltarAdmin()
    {
        $url = \Modulos\AdminAutoDesenvolve\BreadCrumb\Models\Repositorios\AdminAutoDesenvolveBreadCrumbRP::getLastBreadCrumb();
        return isset($url['url']) ? $url['url'] : self::Admin();
    }

    public static function getGetters($excludeGetter = array())
    {
        $urlGetters = '?';
        $excludeGetters = array();
        if (input_get()) {
            if (!empty($excludeGetter)) {
                foreach ($excludeGetter as $exclude) {
                    $excludeGetters[$exclude] = '';
                }
            }
            foreach (input_get() as $key => $value) {
                if (!isset($excludeGetters[$key]) && $key != 'url') {
                    $urlGetters .= $key . '=' . $value . '&';
                }
            }
        }
        $urlGetters = substr($urlGetters, 0, -1);
        return $urlGetters;
    }

}