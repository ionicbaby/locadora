<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 08/11/2016
 * Time: 10:39
 */

namespace Aplicacao;

use Gregwar\Image\Image;

class Imagem
{
    /**
     * @param $image
     * @param $x
     * @param $y
     * @param null $background
     * @param string $type
     * @return string
     */
    public static function resize($image, $x, $y, $background = null, $type = 'jpg')
    {
        if (!file_exists($image)) {
            return $image;
        }
        $imagemName = substr(sha1($image.$x.$y.$background.$type), 0, 8) . '-' . $x . 'X' . $y.'-'.str_replace('#', '', $background);
        $newImage = LI_IMAGES_CACHES . $imagemName . '.' . $type;
        if (file_exists($newImage)) {
            return $newImage;
        }
        $imagem = Image::open($image);
        $imagem->setCacheDir('src/Assets/Images/Caches');
        $imagem->setCacheDirMode(1);
        $imagem->setFallback(null);
        if ($background) {
            $imagem = $imagem
                ->resize($x, $y, $background, true, true, false);
        } else {
            $imagem = $imagem
                ->resize($x, $y, 'transparent', true, true, true);
        }
        $imagem = $imagem->save($newImage);
        return $imagem;
    }
}