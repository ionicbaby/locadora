<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 16/06/2017
 * Time: 16:23
 */

namespace Aplicacao;


use Models\Leitura\ImapParser;

class Leitura
{
    public static function Arquivo()
    {
    }

    /**
     * @param $domain
     * @param $username
     * @param $password
     * @return array
     */
    public static function Email($domain = '{srv152.prodns.com.br:993}INBOX', $username = '	comercial@ligadoscomunicacao.com.br',
                                 $password = '110120', $limit = 10, $offset = 0)
    {
        $email = new ImapParser();
        $data = array(
            'email' => array(
                'hostname' => $domain,
                'username' => $username,
                'password' => $password
            ),
            'pagination' => array(
                'sort' => 'DESC', // or ASC
                'limit' => $limit,
                'offset' => $offset
            )
        );
        $result = $email->inbox($data);
        return $result;
    }

    public static function EmailReturnFailure()
    {
        $emails = self::Email();
        $inbox = isset($emails['inbox']) ? $emails['inbox'] : array();
        $emailsFailure = array();
        foreach ($inbox as $emailRecived) {
            if (isset($emailRecived['subject']) && $emailRecived['subject'] == 'Delivery Status Notification (Failure)') {
                $toEmail = "Your message wasn't delivered to ";
                $sizeToEmail = strlen($toEmail);
                $offsetStart = (strpos($emailRecived['message'], $toEmail) + $sizeToEmail);
                $newBody = substr($emailRecived['message'], $offsetStart);
                $endEmail = "because the address couldn't be found";
                $offsetEnd = (strpos($newBody, $endEmail)) - 1;
                $emailsFailure[] = substr($newBody, 0, $offsetEnd);
            }
        }
        return $emailsFailure;
    }
}