<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 15/11/2016
 * Time: 21:28
 */

namespace Aplicacao\Ferramentas;


class Ajax extends ResponseCode
{

    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const PATCH = 'PATCH';

    /**
     * @param $retorno
     * @param bool $exit
     */
    public static function retorno($retorno, $exit = true)
    {
        echo json_encode($retorno);
        if (!$exit) {
            return;
        }
        exit;
    }

    public static function getHeader($name = false)
    {
        if (!$name) {
            $headers = [];
            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
            return $headers;
        }
        if (isset($_SERVER['HTTP_' . strtoupper($name)])) {
            return $_SERVER['HTTP_' . strtoupper($name)];
        }
        if (isset($_SERVER[$name])) {
            return $_SERVER[$name];
        }
        return false;
    }

    /**
     * @param $tipo
     * @return bool
     */
    public static function setRequestTipo($tipo)
    {
        if ($tipo != self::GET && $tipo != self::POST && $tipo != self::PUT && $tipo != self::DELETE) {
            return false;
        }
        $_SERVER['REQUEST_METHOD'] = $tipo;
        return $_SERVER['REQUEST_METHOD'];
    }

    public static function setResponseCode($responseCode = 200)
    {
        http_response_code($responseCode);
    }

    public static function getResponseCode()
    {
        return http_response_code();
    }

    /**
     * @return mixed
     */
    public static function getRequestTipo()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}