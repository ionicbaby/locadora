<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 12/08/2017
 * Time: 16:04
 */

namespace Aplicacao\Ferramentas;


use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysLogEntidade as SysLog;

class Log
{
    public static function Geral($level = 1, $mensagem = '')
    {
        $level = $level < 1 ? 1 : $level;
        if (!$mensagem) {
            return;
        }
        $level = $level > 10 ? 10 : $level;
        (new SysLog())->setLevel($level)->setMensagem($mensagem)->setInsertData(date('Y-m-d H:i:s'))->insert();
    }
}