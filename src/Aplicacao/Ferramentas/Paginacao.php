<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 24/07/2017
 * Time: 11:20
 */

namespace Aplicacao\Ferramentas;


use Aplicacao\Url;

class Paginacao
{


    public $base_url = '';
    public $base_url_extensions = '';

    public $total_rows = 0;
    public $per_page = 10;

    public $active_background = '';
    public $active_border = '';
    public $active_color = '';
    public $pos_fixo = '';

    /**
     * pagination constructor.
     * @param array $params
     * @usability   $configPagination['base_url'] = base_admin('modulo/listar');
                    $configPagination['per_page'] = $perpage;
                    $configPagination['total_rows'] = $quantidadeConteudoModulo->total
     */
    public function __construct($params = array())
    {
        if (count($params) > 0) {
            $this->initialize($params);
        } else {
            return '';
        }
    }

    /**
     * @param array $params
     */
    function initialize($params = array())
    {
        if (count($params) > 0) {
            foreach ($params as $key => $val) {
                if (isset($this->$key)) {
                    $this->$key = $val;
                }
            }
        }
    }

    /**
     * @return string
     */
    function create_links()
    {
        $totalNumberLinks = ceil($this->total_rows / $this->per_page);
        $prevNumverPage = (input_get('paginacao_ad') - 1) < 0 ? 0 : (input_get('paginacao_ad') - 1);
        $nextNumverPage = (input_get('paginacao_ad') + 1) > $totalNumberLinks ? $totalNumberLinks : (input_get('paginacao_ad') + 1);

        $html = '<nav aria-label="Page navigation">';
        $html .= '<ul class="pagination">';
        $html .= '<li class="back"><a href="' . $this->base_url . '/' . $this->base_url_extensions . '/' . Url::getGetters(array('paginacao_ad')) . '&paginacao_ad=' . $prevNumverPage . '"><span style="color:' . $this->active_background . ';" aria-hidden="true">&laquo;</span></a></li>';

        if ($totalNumberLinks) {
            for ($i = 0; $i < $totalNumberLinks; $i++) {
                $active = input_get('paginacao_ad') == $i ? 'active' : '';
                $style = input_get('paginacao_ad') == $i ? 'color:' . $this->active_color . '; background-color:' . $this->active_background . '; border:1px solid ' . $this->active_border : 'color:' . $this->active_border . '; ';
                $html .= '<li class="' . $active . '"><a style="' . $style . '" href="' . $this->base_url . '/' . ($this->base_url_extensions ? $this->base_url_extensions.'/' : '') . (Url::getGetters(array('paginacao_ad')) ? Url::getGetters(array('paginacao_ad')).'&' : '?') . 'paginacao_ad=' . $i . $this->pos_fixo.'">' . ($i + 1) . '</a></li>';
            }
        } else {
            $html .= '<li><a href="' . $this->base_url . '/' . $this->base_url_extensions . '/' . Url::getGetters(array('paginacao_ad')) . '&paginacao_ad=' . 0 . '">' . 1 . '</a></li>';
        }
        $html .= '<li class="next"><a href="' . $this->base_url . '/' . $this->base_url_extensions . '/' . Url::getGetters(array('paginacao_ad')) . '&paginacao_ad=' . $nextNumverPage . '"><span style="color:' . $this->active_background . ';" aria-hidden="true">&raquo;</span></a></li>';
        $html .= '</ul>';
        $html .= '</nav>';
        return str_replace('http:/','http://', str_replace('//','/', $html));
    }

    public function getOffset()
    {
        $start = 0;
        $perpage = $this->per_page;
        if (input_get('paginacao_ad')) {
            $start = input_get('paginacao_ad');
        }
        $start = $start > 0 ? ($start * $perpage) : 0;
        return array('start' => $start, 'end' => $perpage);
    }
}