<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 09/11/2016
 * Time: 21:14
 */

namespace Aplicacao\Ferramentas;


use Aplicacao\Url;

class GetAssets
{

    /**
     * @param string $arquivoJs
     * @param null $grupoModulo
     * @param null $nomeModulo
     */
    public static function JS($arquivoJs = '', $grupoModulo = null, $nomeModulo = null)
    {
        $url = 'src/Assets/Js/' . $arquivoJs . '.js';
        echo '<script type="text/javascript" src="' . Url::Base($url) . '"></script>';
    }

    /**
     * @param string $arquivoCss
     * @param bool $urlOnly
     * @return string
     */
    public static function CSS($arquivoCss = '', $urlOnly = false)
    {
        $url = 'src/Assets/Css/' . $arquivoCss . '.css';
        if($urlOnly){
            return $url;
        }
        echo '<link rel="stylesheet" type="text/css" href="' . Url::Base($url) . '" />';
        return '';
    }

    /**
     * @param string $file
     * @param bool $urlOnly
     * @return string
     */
    public static function Plugins($file = '', $urlOnly = false)
    {
        $url = 'src/Assets/Plugins/' . $file;
        if($urlOnly){
            return $url;
        }
        echo Url::Base($url);
        return '';
    }

    public static function IMAGE($imagem = '')
    {
        $url = 'src/Assets/Images/' . $imagem;
        if (!is_file($url)) {
            $url = 'src/Assets/Images/Default/imagem-nao-encontrada.jpg?realfile=' . $url;
        }
        return $url;
    }


}