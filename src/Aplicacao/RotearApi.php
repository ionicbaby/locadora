<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 00:03
 */

namespace Aplicacao;

use Api\ApiExeption;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Ferramentas\ResponseCode;

class RotearApi
{
    private $url;

    private $classe_api;

    private $grupo_modulo;
    private $nome_modulo;
    private $classe_controlador;

    private $metodo = 'init';
    private $offset_contar_atributos = 4;
    private $offset_metodo = 3;
    private $atributos_metodo = array();

    public function init($url)
    {
        $this->url = $url;
        $this->gerarClasseDaApi();
        $this->gerarAtributos();
        $this->rotearCamadasApi();
    }

    private function gerarClasseDaApi()
    {
        $this->grupo_modulo = isset($this->url[1]) ? ucfirst($this->url[1]) . 'API' : false;
        $this->nome_modulo = isset($this->url[2]) ? ucfirst($this->url[2]) : false;
        $this->classe_controlador = isset($this->url[3]) ? ucfirst($this->url[3]) : 'Index';
        $this->offset_contar_atributos = 5;
        $classe = 'Modulos\\' . $this->grupo_modulo . '\\' . $this->nome_modulo . '\\Controllers\\' . $this->classe_controlador;

        if (!class_exists($classe)) {
            $this->offset_contar_atributos = 4;
            $this->classe_controlador = 'Index';
            $classe = 'Modulos\\' . $this->grupo_modulo . '\\' . $this->nome_modulo . '\\Controllers\\' . $this->classe_controlador;
        }
        $this->offset_metodo = ($this->offset_contar_atributos - 1);
        $this->metodo = isset($this->url[$this->offset_metodo]) ? $this->url[$this->offset_metodo] : 'NaoEncontrado';
        $this->metodo = ucfirst($this->metodo.Ajax::getRequestTipo());

        if (!method_exists($classe, $this->metodo)) {
            $this->offset_metodo = false;
            $this->offset_contar_atributos = 3;
            $this->metodo = 'init';
        }

        $this->classe_api = $classe;
    }

    private function gerarAtributos()
    {
        foreach($this->url as $key => $atributo){
            if($key >= $this->offset_contar_atributos){
                $this->atributos_metodo[] = $atributo;
            }
            continue;
        }
    }

    private function rotearCamadasApi()
    {
        try {
            if (!class_exists($this->classe_api)) {
                throw new ApiExeption('Classe não encontrada: '.$this->classe_api);
            }
            if (!method_exists($this->classe_api, $this->metodo)) {
                throw new ApiExeption('Metodo não encontrada: '.$this->metodo);
            }
        } catch (ApiExeption $e) {
            $json = array(
                'msg' => $e->getMessage(),
                'acao' => $e->getAction(),
                'erro' => true
            );
            Response::json($json, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        $classeInstancia = new $this->classe_api();
        call_user_func_array(array($classeInstancia, $this->metodo), $this->atributos_metodo);
        return;
    }


}