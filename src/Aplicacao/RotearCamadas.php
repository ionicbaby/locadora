<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 10:25
 */

namespace Aplicacao;


use Aplicacao\Ferramentas\Ajax;
use Controllers\IndexControlador;
use Core\Modelos\Carregamentos;

class RotearCamadas
{

    private $url;

    private $metodo = 'index';
    private $atributos_metodo = array();


    public function __construct()
    {
        $this->rotearControlador();
    }

    /**
     * @description RotearCamadas
     */
    private function rotearControlador()
    {
        $get = filter_input(INPUT_GET, 'url');

        $urlGet = \Aplicacao\Url::GetUrlRealPorUrlApelido(rtrim($get, '/'));
        //show_array($urlGet, true);
        $partsUrl = parse_url($urlGet);
        $originalGet = $_GET;

        if (isset($partsUrl['query'])) {
            parse_str($partsUrl['query'], $_GET);
        }

        $_GET = array_merge($originalGet, $_GET);
        if (strpos($urlGet, '?')) {
            $urlGet = substr($urlGet, 0, strpos($urlGet, '?'));
        }
        $this->url = explode('/', $urlGet);
        $_SERVER['QUERY_STRING'] = $urlGet;

        // Rota Index Padrão Site
        if (!$get) {
            $this->instanciarIndexPadrao();
            return;
        }

        // Rota Padrão Modulos
        if (
            (count($this->url) >= 4 && ($this->url[0] == 'Modulo')) ||
            (count($this->url) >= 5 && ($this->url[0] == 'Admin' || $this->url[0] == 'admin') && ($this->url[1] == 'Modulo'))
        ) {
            if ($this->url[0] == 'Admin' || $this->url[0] == 'admin') {
                $this->instanciarRotaModulo(true);
                return;
            }
            $this->instanciarRotaModulo(false);
            return;
        }

        //Rota Padrão Api
        if (isset($this->url[0]) && ($this->url[0] == 'API' || $this->url[0] == 'Api' || $this->url[0] == 'api')) {
            $this->instanciarRotaApi();
            return;
        }

        // Rota Padrão Site
        $this->instanciarRotaDinamica();
        return;
    }

    private function instanciarIndexPadrao()
    {
        $classe = new IndexControlador();
        $classe->index();
    }

    private function instanciarRotaModulo($admin = false)
    {
        $offsetInicial = 0;
        if ($admin) {
            $offsetInicial += 1;
        }
        if (isset($this->url[$offsetInicial])) {
            if (count($this->url) >= ($offsetInicial + 4)) {
                if (isset($this->url[($offsetInicial + 4)])) {
                    $this->metodo = $this->url[($offsetInicial + 4)];
                } else {
                    $this->metodo = 'index';
                }
            }
            $namespaceModulo = '';
            foreach ($this->url as $key => $uri) {
                if ($key >= $offsetInicial && $key <= ($offsetInicial + 3)) {
                    if ($uri == 'Modulo' && $offsetInicial == $key) {
                        $uri = 'Modulos';
                    }
                    if ($key == ($offsetInicial + 3)) {
                        $namespaceModulo .= 'Controllers\\';
                    }
                    $namespaceModulo .= $uri . '\\';
                }
            }

            $Classe = substr($namespaceModulo, 0, -1);

            if (!class_exists($Classe)) {
                $carregamento = new Carregamentos();
                Ajax::setResponseCode(Ajax::HTTP_NOT_FOUND);
                $dadosView['pagina'] = 'Home/404';
                $dadosView['mensagem'] = 'Não foi encontrado o controlador: <b>' . $Classe . '</b>';
                $carregamento->view('Home/index', $dadosView);
                return;
            }

            $this->atributos_metodo = $this->getAtributor($offsetInicial);
            $instanciaDaClasse = new $Classe();

            if (!method_exists($instanciaDaClasse, $this->metodo)) {
                $this->atributos_metodo = $this->getAtributor(($offsetInicial - 1));
                $this->metodo = 'index';
            }


            if (method_exists($instanciaDaClasse, $this->metodo)) {
                call_user_func_array(array($instanciaDaClasse, $this->metodo), $this->atributos_metodo);
            } else {
                $mensagem = 'Não foi encontrado o metodo: <b>' . $this->metodo . '.</b> Enquanto roteava a classe: <b>' . $Classe . '</b>';
                $carregamento = new Carregamentos();
                $dadosView['pagina'] = 'Home/404';
                $dadosView['mensagem'] = $mensagem;
                $carregamento->view('Home/index', $dadosView);
                return;
            }
        }
    }

    private function getAtributor($offset = 0)
    {
        $atributos = array();
        foreach ($this->url as $key => $segment) {
            if (($key > ($offset + 3)) && count($this->url) <= ($offset + 3)) {
                $atributos[] = $segment;
            } elseif ($key > ($offset + 4) && count($this->url) > ($offset + 4)) {
                $atributos[] = $segment;
            }
        }
        return $atributos;
    }

    private function instanciarRotaDinamica($admin = false)
    {
        $offsetInicial = 0;
        if ($admin) {
            $offsetInicial = 1;
            if (!isset($this->url[$offsetInicial])) {
                $this->url[$offsetInicial] = 'IndexControlador';
            }
        }


        if (isset($this->url[$offsetInicial])) {

            if (count($this->url) >= ($offsetInicial + 2)) {
                $this->metodo = $this->url[($offsetInicial + 1)];
            }
            if (count($this->url) < ($offsetInicial + 1)) {
                $this->metodo = $this->url[0];
            }

            foreach ($this->url as $key => $segment) {
                if ($key > $offsetInicial && count($this->url) <= ($offsetInicial + 2)) {
                    $this->atributos_metodo[] = $segment;
                } elseif ($key > ($offsetInicial + 1) && count($this->url) > ($offsetInicial + 2)) {
                    $this->atributos_metodo[] = $segment;
                }
            }

            $Classe = LI_NAMESPACE_CONTROLLERS . $this->url[$offsetInicial];


            if ($admin) {
                $Classe = LI_NAMESPACE_CONTROLLERS_ADMIN . $this->url[$offsetInicial];
            }

            if (!class_exists($Classe)) {
                $carregamento = new Carregamentos();
                Ajax::setResponseCode(Ajax::HTTP_NOT_FOUND);
                $dadosView['pagina'] = 'Home/404';
                $dadosView['mensagem'] = 'Não foi encontrado o controlador: <b>' . $Classe . '</b>';
                $carregamento->view('Home/index', $dadosView);
                return;
            }
            $instanciaDaClasse = new $Classe();

            if (!method_exists($instanciaDaClasse, $this->metodo)) {
                foreach ($this->atributos_metodo as $key => $valorAtributo) {
                    $this->atributos_metodo[($key + 1)] = $valorAtributo;
                }
                $this->atributos_metodo[0] = $this->url[1];
                $this->metodo = 'index';
            }

            if (method_exists($instanciaDaClasse, $this->metodo)) {
                call_user_func_array(array($instanciaDaClasse, $this->metodo), $this->atributos_metodo);
            } else {
                $mensagem = 'Não foi encontrado o metodo: <b>' . $this->metodo . '.</b> Enquanto roteava a classe: <b>' . $Classe . '</b>';
                $carregamento = new Carregamentos();
                $dadosView['pagina'] = 'Home/404';
                $dadosView['mensagem'] = $mensagem;
                $carregamento->view('Home/index', $dadosView);
                return;
            }
        }
    }

    private function instanciarRotaApi()
    {
        $roteadorDeAplicacoesApi = new RotearApi();
        $roteadorDeAplicacoesApi->init($this->url);
    }


}