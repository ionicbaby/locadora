<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 16/03/2018
 * Time: 15:39
 */

namespace Models\EndPoints;


use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Models\EndPoints\Abstracts\DefaultEndPoints;

class BuscarDadosTipoVeiculo extends DefaultEndPoints
{
    public function get()
    {
        Ajax::setResponseCode(Response::HTTP_OK);
        return array(
            'dados' => array(
                'carros'=>true,
            ));
    }
}

