<?php
/**
 * Created by PhpStorm.
 * User: FRAG
 * Date: 29/03/2018
 * Time: 10:44
 */

namespace Models\EndPoints;

use Models\EndPoints\Abstracts\DefaultEndPoints;

class buscarCategoriaVeiculo extends DefaultEndPoints
{
    public function get()
    {

        $dadosDaCategoria = $this->db->select('descricao')->where('categoria_id', input_get('categoria_id'))->get('valores_categoria')->result(true);
        return array(
            'dados' => (array)$dadosDaCategoria
        );
    }

}