<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 16/03/2018
 * Time: 15:44
 */

namespace Models\EndPoints\Abstracts;


use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Core\Modelos\ModelagemDb;

abstract class DefaultEndPoints
{
    /**
     * @var ModelagemDb
     */
    public $db;
    public function __construct()
    {
        $this->db = new ModelagemDb();
    }

    public function get()
    {
        Ajax::setResponseCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        return array('dados' => 'Faltou o metodo get do endpoint');
    }
}