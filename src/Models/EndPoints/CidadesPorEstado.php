<?php
/**
 * Created by PhpStorm.
 * User: FRAG
 * Date: 29/03/2018
 * Time: 14:39
 */

namespace Models\EndPoints;


use Models\EndPoints\Abstracts\DefaultEndPoints;

class CidadesPorEstado extends DefaultEndPoints
{
    public function get()
    {
        $cidadesDoEstado = $this->db
            ->select('ci.*')
            ->join('locadora_clientes lc', 'lc.id = ca.cliente_id')
            ->join('sys_cidades ci', 'ci.codigo = ca.cidade_id')
            ->group_by('ca.cidade_id')
            ->where('ci.sigla_uf', input_get('sigla_estado'))
            ->get('cidades_clientes_atendem ca')
            ->result(false);
        return array(
            'dados' => (array)$cidadesDoEstado
        );
    }


}