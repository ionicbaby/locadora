<?php
/**
 * Created by PhpStorm.
 * User: Shared
 * Date: 26/04/2018
 * Time: 20:32
 */

namespace Models\EndPoints;


use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Url;
use Models\EndPoints\Abstracts\DefaultEndPoints;
use Models\EnvioEmail\EnvioMailer;

class Cron extends DefaultEndPoints
{

    private $orcamentosPendentes = array();
    private $dadosDoServidor = array();

    public function __construct()
    {
        set_time_limit(0);
        parent::__construct();
        $this->setDadosDoServidor();
        $this->orcamentosPendentes = $this->db->select('*')->where('id_cidade IS NOT NULL', '', true)->where('email_enviado', '0')->get('orcamento')->result(false);
    }

    private function setDadosDoServidor()
    {
        $this->dadosDoServidor = array(
            'seguranca' => 'ssl',
            'host' => 'srv152.prodns.com.br',
            'porta' => 465,
            'username' => 'pedidos_ligacao@ligadoscomunicacao.com.br',
            'password' => '110120',
            'from' => 'pedidos_ligacao@ligadoscomunicacao.com.br',
            'nome_from' => 'ALUGARCARRO.SITE',
            'reply' => 'pedidos_ligacao@ligadoscomunicacao.com.br'
        );
    }

    public function executar()
    {
        $envios = array();

        foreach ($this->orcamentosPendentes as $orcamento) {

            $objetoDadosCliente = new \stdClass();

            if (Identificadores::eJson($orcamento->dados_cliente)) {
                $objetoDadosCliente = json_decode($orcamento->dados_cliente);
            }

            $dadosCliente['sigla_uf'] = isset($objetoDadosCliente->sigla_uf) ? $objetoDadosCliente->sigla_uf : 'Não identificado';
            $dadosCliente['data_retirada'] = isset($objetoDadosCliente->data_retirada) ?
                date('d/m/Y H:i', strtotime($objetoDadosCliente->data_retirada)) : 'Não identificado';
            $dadosCliente['data_entrega'] = isset($objetoDadosCliente->data_entrega) ?
                date('d/m/Y H:i', strtotime($objetoDadosCliente->data_entrega)) : 'Não identificado';
            $dadosCliente['nome'] = isset($objetoDadosCliente->nome) ? $objetoDadosCliente->nome : 'Não identificado';
            $dadosCliente['email'] = isset($objetoDadosCliente->email) ? $objetoDadosCliente->email : 'Não identificado';
            $dadosCliente['ddd'] = isset($objetoDadosCliente->ddd) ? $objetoDadosCliente->ddd : 'Não identificado';
            $dadosCliente['telefone'] = isset($objetoDadosCliente->telefone) ? $objetoDadosCliente->telefone : 'Não identificado';
            $dadosCliente['operadora'] = isset($objetoDadosCliente->operadora) ? $objetoDadosCliente->operadora : 'Não identificado';
            $dadosCliente['tipo_contato'] = isset($objetoDadosCliente->tipo_contato) ?
                $objetoDadosCliente->tipo_contato : 'Não identificado';
            $dadosCliente['opcionais'] = isset($objetoDadosCliente->opcionais) ?
                $objetoDadosCliente->opcionais : 'Não identificado';
            $dadosCliente['categoria_carro'] = isset($objetoDadosCliente->categoria_carro) ?
                $objetoDadosCliente->categoria_carro : 'Não identificado';
            $locadorasDaCidade = $this->db
                ->select('lc.*, ca.cidade_id')
                ->join('locadora_clientes lc', 'lc.id = ca.cliente_id')
                ->where('ca.cidade_id', $orcamento->id_cidade)
                ->group_by('ca.cliente_id')
                ->get('cidades_clientes_atendem ca')->result(false);

            foreach ($locadorasDaCidade as $locadora) {
                $quantidadeEmailsEnviados = $this->db
                    ->select('count(1) as total')
                    ->where('em.id_locadora', $locadora->id)
                    ->get('emails_enviados em')
                    ->row(false);
                $dadosLocadora['nome'] = $locadora->nome;
                $dadosLocadora['email'] = $locadora->email_propostas;
                $dadosLocadora['quantidade_email_enviados'] = $quantidadeEmailsEnviados->total + 1;
                try {
                    $envioEmail = $this->enviarEmail($dadosCliente, $dadosLocadora);
                    if (!$envioEmail) {
                        Ajax::setResponseCode(Response::HTTP_BAD_GATEWAY);
                        return array('dados' => array('msg' => 'Algo deu errado ao rnviar e-mails.'));
                    }
                    $this->marcarEmailEnviadoDaLocadora($locadora->id, $orcamento->id);
                } catch (\Exception $e) {
                    Ajax::setResponseCode(Response::HTTP_BAD_GATEWAY);
                    return array('dados' => array('msg' => $e->getMessage()));
                }
            }
            $this->setOrcamentoJaEnviado($orcamento->id);
        }
        return $envios;
    }

    private function enviarEmail($dadosCliente, $dadosLocadora)
    {
        $novoOrcamentoMensagem = 'Bom dia ';
        if (date('H') >= 12) {
            $novoOrcamentoMensagem = 'Boa tarde ';
        }
        if (date('H') >= 19) {
            $novoOrcamentoMensagem = 'Boa noite ';
        }
        $novoOrcamentoMensagem = '<b>' . $novoOrcamentoMensagem;


        $corpoDoEmail = '<img style="max-width:250px;" src="https://staticaltmetric.s3.amazonaws.com/uploads/2015/10/dark-logo-for-site.png" alt="locadora"><br><br>';
        $corpoDoEmail .= $novoOrcamentoMensagem . ' ' . $dadosLocadora['nome'] . '.</b><br><br><b>Solicitação de Orçamento: </b>' . Url::Base() . ' <br><br>';
        $corpoDoEmail .= '<b>Dados do possível cliente:</b><br>';
        $corpoDoEmail .= '<b>Nome:</b> ' . $dadosCliente['nome'] . '<br>';
        $corpoDoEmail .= '<b>Contato:</b> ' . $dadosCliente['operadora'] . ' (' . $dadosCliente['ddd'] . ') ' . $dadosCliente['telefone'] . '<br>';
        $corpoDoEmail .= '<b>E-mail:</b> ' . $dadosCliente['email'] . '<br>';
        $corpoDoEmail .= '<b>Tipo de Contato Preferencial :</b>' . $dadosCliente['tipo_contato'] . '<br><br>';
        $corpoDoEmail .= '<b>Dados do carro:</b><br><br>';
        $corpoDoEmail .= '<b>Categoria do carro:</b> ' . $dadosCliente['categoria_carro'] . '<br>';
        $corpoDoEmail .= '<b>Data de retirada:</b> ' . $dadosCliente['data_retirada'] . '<br>';
        $corpoDoEmail .= '<b>Data de entrega:</b> ' . $dadosCliente['data_entrega'] . '<br>';
        $corpoDoEmail .= '<b>Opcionais:</b> ' . $dadosCliente['opcionais'] . '<br><br><hr><br>';
        $corpoDoEmail .= '<b> Total de orçamentos já enviados para você: <span style="color: #0099ff;">' . $dadosLocadora['quantidade_email_enviados'] . '</span><b>';

        $mailer = new EnvioMailer();
        $retornosDeEnvios[] = $mailer->enviarEmailSMTP(
            $this->dadosDoServidor,
            $dadosLocadora['email'],
            $dadosLocadora['nome'],
            'ORÇAMENTO - ' . ' PROTOCOLO: ' . rand(10000, 99999),
            $corpoDoEmail,
            array());
        return $retornosDeEnvios;
    }

    private function setOrcamentoJaEnviado($idOrcamento)
    {
        $dados['id'] = $idOrcamento;
        $dados['email_enviado'] = 1;
        $this->db->update('orcamento', $dados);
    }

    /**
     * @param $idLocadora
     * @param $idOrcamenti
     * @return Bool
     */
    private function marcarEmailEnviadoDaLocadora($idLocadora, $idOrcamento)
    {
        $dados['id_locadora'] = $idLocadora;
        $dados['id_orcamento'] = $idOrcamento;
        return $this->db->insert('emails_enviados', $dados);
    }

}