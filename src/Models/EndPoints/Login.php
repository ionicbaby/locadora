<?php
/**
 * Created by PhpStorm.
 * User: Shared
 * Date: 30/04/2018
 * Time: 20:00
 */

namespace Models\EndPoints;


use Aplicacao\Ferramentas\Ajax;
use Models\EndPoints\Abstracts\DefaultEndPoints;

class Login extends DefaultEndPoints
{
    public function get()
    {
        $emailExiste = $this->db
            ->select('count(1) as total')
            ->where('email_usuario', input_get('usuario'))
            ->get('usuarios')
            ->row(false);

        if (!isset($emailExiste->total) || $emailExiste->total == 0) {
            Ajax::setResponseCode(Ajax::HTTP_INTERNAL_SERVER_ERROR);
            return array('msg' => 'Usuário não foi encontrado!');
        }

        $senhaEmailExiste = $this->db
            ->select('count(1) as total')
            ->where('email_usuario', input_get('usuario'))
            ->where('senha_usuario', input_get('senha'))
            ->get('usuarios')
            ->row(false);

        if (!isset($senhaEmailExiste->total) || $senhaEmailExiste->total == 0) {
            Ajax::setResponseCode(Ajax::HTTP_INTERNAL_SERVER_ERROR);
            return array('msg' => 'Senha incorreta!');
        }

        $dadosDousuario = $senhaEmailExiste = $this->db
            ->where('email_usuario', input_get('usuario'))
            ->where('senha_usuario', input_get('senha'))
            ->get('usuarios')
            ->row(false);

        $dadosLocadora = null;
        if ($dadosDousuario->acl_usuario == 2 && $dadosDousuario->locadora_id >= 1) {
            $dadosLocadora = $this->db->where('id', $dadosDousuario->locadora_id)->get('locadora_clientes')->row(false);
        }

        $_SESSION['admin'] = array(
            'dados_usuario' => $dadosDousuario,
            'tipo_acesso' => $dadosDousuario->acl_usuario,
            'dados_locadora' => $dadosLocadora
        );
        return array(
            'msg' => 'Logado como ' . ($dadosDousuario->acl_usuario == 1 ? 'administração' : $dadosLocadora->nome)
        );
    }

}