<?php
/**
 * Created by PhpStorm.
 * User: Shared
 * Date: 01/05/2018
 * Time: 19:29
 */

namespace Models\EndPoints;


use Models\EndPoints\Abstracts\DefaultEndPoints;

class GetValoresRelatorio extends DefaultEndPoints
{
    /**
     * @return array
     *
     */
    public function getTotalEmailsEnviados()
    {
        $graficos = $this->db
            ->select('COUNT(1) AS total_emails')
            ->get('emails_enviados')
            ->row(false);
        return array(
            'dados' => (isset($graficos->total_emails) ? $graficos->total_emails : 0)
        );
    }

    public function getTotalCadastroLocadoras()
    {
        $cadastros = $this->db
            ->select('COUNT(1) AS total_locadoras')
            ->get('locadora_clientes')
            ->row(false);
        return array(
            'locadoras' => (isset($cadastros->total_locadoras) ? $cadastros->total_locadoras : 0)
        );
    }

    public function getTotalClientesAtendidos()
    {
        $total_clientes = $this->db
            ->select('COUNT(1) AS total_clientes')
            ->get('cadastro_usuario')
            ->row(false);
        return array(
            'totalClientes' => (isset($total_clientes->total_clientes) ? $total_clientes->total_clientes : 0)
        );
    }

    public function getTopDezEmailsEnviados()
    {
        $resultado = $this->db
            ->select(' lc.id, lc.nome, COUNT(1) total')
            ->join('locadora_clientes lc', 'lc.id = ev.id_locadora')
            ->where('ev.data_envio BETWEEN CAST(SUBDATE(NOW(), INTERVAL 1 WEEK) AS CHAR(19)) AND CAST(NOW() AS CHAR(19))', '',true)
            ->group_by('ev.id_locadora')
            ->order_by('total', 'DESC')
            ->limit(10)
            ->get('emails_enviados ev')
            ->result();
        return $resultado;
    }
}