<?php
/**
 * Created by PhpStorm.
 * User: FRAG
 * Date: 29/03/2018
 * Time: 18:16
 */

namespace Models\EndPoints;


use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Url;
use Models\EndPoints\Abstracts\DefaultEndPoints;
use Models\EnvioEmail\EnvioMailer;

class SalvarDadosFormulario extends DefaultEndPoints
{
    public function post()
    {
        $formatoDataRetirada = (new \DateTime(date('Y-m-d')))->format('Y-m-d H:i:s');
        $formatoDataEntrega = (new \DateTime(date('Y-m-d')))->format('Y-m-d H:i:s');

        if (input_post('data_horiario_retirada')) {
            $dataRetirada = (explode(' ', input_post('data_horiario_retirada')))[0];
            $horarioRetirada = (explode(' ', input_post('data_horiario_retirada')))[1];
            $etapasDataRetirada = (explode('/', $dataRetirada));
            $formatoDataRetirada = $etapasDataRetirada[2] . '-' . $etapasDataRetirada[1] . '-' . $etapasDataRetirada[0] . ' ' . $horarioRetirada;
            $formatoDataRetirada = (new \DateTime($formatoDataRetirada))->format('Y-m-d H:i:s');
        }
        if (input_post('data_horiario_entrega')) {
            $dataEntrega = (explode(' ', input_post('data_horiario_entrega')))[0];
            $horarioEntrega = (explode(' ', input_post('data_horiario_entrega')))[1];
            $etapasDataEntrega = (explode('/', $dataEntrega));
            $formatoDataEntrega = $etapasDataEntrega[2] . '-' . $etapasDataEntrega[1] . '-' . $etapasDataEntrega[0] . ' ' . $horarioEntrega;
            $formatoDataEntrega = (new \DateTime($formatoDataEntrega))->format('Y-m-d H:i:s');
        }

        $data['sigla_uf'] = input_post('estados');
        $data['codigo_cidade'] = input_post('cidades');
        $data['data_retirada'] = $formatoDataRetirada;
        $data['data_entrega'] = $formatoDataEntrega;
        $data['nome'] = input_post('nome_completo');
        $data['email'] = input_post('email');
        $data['ddd'] = input_post('ddd');
        $data['telefone'] = input_post('telefone');
        $data['operadora'] = input_post('operadora');
        $data['tipo_contato'] = input_post('tipo_contato');
        $data['categoria_carro'] = input_post('categoria_carro');
        $data['opcionais'] = is_array(input_post('opcionais')) ? implode(', ', input_post('opcionais')) : null;

        $cadastrado = $this->db->insert('cadastro_usuario', $data);
        if (!$cadastrado) {
            Ajax::setResponseCode(Response::HTTP_BAD_GATEWAY);
            return array('dados' => array('msg' => 'Algo deu errado ao cadastrar seu pedido! Por favor tente novamente, se o erro persistir entre em contato'));
        }
        $daosOrcamento['id_cidade'] = $data['codigo_cidade'];
        $daosOrcamento['dados_cliente'] = json_encode($data);
        $envioOrcamento = $this->db->insert('orcamento', $daosOrcamento);
        if (!$envioOrcamento) {
            Ajax::setResponseCode(Response::HTTP_BAD_GATEWAY);
            return array('dados' => array('msg' => 'mensagem de nao gravado o orcamento'));
        }
        Ajax::setResponseCode(Response::HTTP_OK);
        return array('dados' => array('msg' => 'Orçamento solicitado'));
    }
}