<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 08/11/2016
 * Time: 15:45
 */

namespace Core\DB;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class BootstrapConfigORM
{

    /**
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function getManager()
    {
        //Diretório aonde vou guardar as entidades
        $paths = array("src");
        $isDevMode = false;

        // Dados da conexão
        $dbParams = array(
            'driver' => 'pdo_mysql',
            'user' => LI_CONNECT_USER,
            'password' => LI_CONNECT_PASSWORD,
            'dbname' => LI_CONNECT_DATABASE,
        );

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        $entityManager = EntityManager::create($dbParams, $config);
        return $entityManager;
    }

}