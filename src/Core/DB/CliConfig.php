<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 08/11/2016
 * Time: 17:11
 */

namespace Core\DB;


use Doctrine\ORM\Mapping\ClassMetadataInfo;

class CliConfig
{


    /**
     * @return \Symfony\Component\Console\Helper\HelperSet
     */
    public function createHelperSet()
    {
        $entityManager = new BootstrapConfigORM();
        return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager->getManager());
    }

    public function Generate()
    {
        $class[] = new ClassMetadataInfo('Modulos\\Entidades\\Genericas\\Ariza');

        $generator = new \Doctrine\ORM\Tools\EntityGenerator();
        $generator->setGenerateAnnotations(true);
        $generator->setGenerateStubMethods(true);
        $generator->setRegenerateEntityIfExists(true);
        $generator->setUpdateEntityIfExists(true);
        $generator->generate($class, 'src/');
    }

}