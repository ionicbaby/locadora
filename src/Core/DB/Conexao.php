<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 09:37
 */

namespace Core\DB;
use \PDO;

class Conexao
{

    private static $dbtype = LI_CONNECT_DBTYPE;
    private static $host = LI_CONNECT_HOST;
    private static $port = LI_CONNECT_PORT;
    private static $user = LI_CONNECT_USER;
    private static $password = LI_CONNECT_PASSWORD;
    private static $db = LI_CONNECT_DATABASE;
    public $lastid;


    /**
     *
     */
    public function __destruct()
    {
        $this->disconnect();
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
    }

    /**
     * @return string
     */
    public function getDBType()
    {
        return self::$dbtype;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return self::$host;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return self::$port;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return self::$user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return self::$password;
    }

    /**
     * @return string
     */
    public function getDB()
    {
        return self::$db;
    }

    public function getCharset(){
        return 'UTF8';
    }

    /**
     * @return PDO
     */
    private function connect()
    {
        try {
            $dsn = $this->getDBType() . ":host=" . $this->getHost() . ";port=" . $this->getPort() . ";dbname=" . $this->getDB() . ";charset=UTF8";
            $this->conexao = new PDO($dsn, $this->getUser(), $this->getPassword());
        } catch (\PDOException $i) {
            //se houver exceção, exibe
            die("Erro: <code>" . $i->getMessage() . "</code>");
        }

        return ($this->conexao);
    }

    /**
     *
     */
    private function disconnect()
    {
        $this->conexao = null;
    }

    /**
     * @return string
     */
    public function dbNome()
    {
        $dbNome = self::$db;

        return $dbNome;
    }

    /**
     * @param $sql
     * @param null $params
     * @param null $class
     * @return array
     */
    public function selectDB($sql, $params = null, $class = null)
    {
        $query = $this->connect()->prepare($sql);
        $query->execute($params);

        if (isset($class)) {
            $rs = $query->fetchAll(PDO::FETCH_CLASS, $class) or die(print_r($query->errorInfo(), true));
        } else {
            $rs = $query->fetchAll(PDO::FETCH_OBJ);
        }
        self::__destruct();
        return $rs;
    }

    /**
     * @param $sql
     * @param null $params
     * @return string
     */
    public function insertDB($sql, $params = null)
    {
        $conexao = $this->connect();
        $query = $conexao->prepare($sql);
        $query->execute($params);
        $error = 'SQL: '.$sql.'; ';
        $error .= implode(',', $query->errorInfo());
        $rs = $conexao->lastInsertId() or die($error);
        self::__destruct();
        $this->lastid = $rs;
        return $this->lastid;
    }

    /**
     * @param $sql
     * @param null $params
     * @return int
     */
    public function updateDB($sql, $params = null)
    {
        $query = $this->connect()->prepare($sql);
        $query->execute($params);
        $rs = $query->rowCount();
        self::__destruct();
        return $rs;
    }

    /**
     * @param $sql
     * @param null $params
     * @return int
     */
    public function deleteDB($sql, $params = null)
    {
        $query = $this->connect()->prepare($sql);
        $query->execute($params);
        $rs = $query->rowCount() or die(print_r($query->errorInfo(), true));
        self::__destruct();
        return $rs;
    }


}