<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 31/03/2017
 * Time: 19:30
 */

namespace Core\Modelos;


use Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuarios;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade;

class Autentificacoes
{

    private $db;

    public function __construct()
    {
        $this->db = new ModelagemDb();
    }

    /**
     * @param $user
     * @param $password
     * @return array
     */
    public function autenticarAdministrador($user, $password)
    {
        $administracaoUsuarioSenha = (new CmsUsuariosEntidade())->getRow(
            $this->db
                ->where('email', $user)
                ->where('senha', $password)
        );

        $administracaoUsuario = (new CmsUsuariosEntidade())->getRow($this->db->where('email', $user));

        if (!$administracaoUsuarioSenha->getId()) {
            $_SESSION['admin'] = false;
            if ($administracaoUsuario->getId()) {
                return array('level' => 1, 'mensagem' => 'Senha incorreta!');
            }
            return array('level' => 1, 'mensagem' => 'Usuário e senha incorretos!');
        }
        
        $atualizarToken = $administracaoUsuario->setToken(sha1(microtime()))->update();
        if(!$atualizarToken){
            return array('level'=>1, 'mensagem'=>'Falha ao criar token');
        }
        $_SESSION['admin'] = $administracaoUsuario;
        $_SESSION['admin_escritorios'] = AdminAutoDesenvolveEscritoriosRP::getEscritoriosDoUsuarioCms();
        $_SESSION['admin_escritorio_atual'] = 0;
        return array('level' => 0, 'mensagem' => 'Redirecionando');
    }

}