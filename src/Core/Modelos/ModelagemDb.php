<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 09:51
 */

namespace Core\Modelos;

use Core\Modelos\Modelagem as ModelagemDriver;
use Illuminate\Database\Capsule\Manager as Capsule;

class  ModelagemDb
{

    /**
     * @var array
     */
    private $where = array();

    /**
     * @var
     */
    private $order;

    /**
     * @var
     */
    private $order_by = '';

    /**
     * @var
     */
    private $group_by = '';

    /**
     * @var
     */
    private $join = array();

    /**
     * @var array
     */
    private $having = array();

    /**
     * @var
     */
    private $limit;

    /**
     * @var ModelagemDriver
     */
    private $LI_Model;

    /**
     * @var bool
     */
    private $select = false;

    /**
     * @var array
     */
    private $options = array();

    /**
     * @var (int)
     */
    private $lastInsertId;

    public function __construct()
    {
        $this->setLI_Model();
    }

    /**
     *
     */
    public function setLI_Model()
    {
        $this->LI_Model = new ModelagemDriver();
    }

    /**
     * @return ModelagemDriver
     */
    public function getLI_Model()
    {
        return $this->LI_Model;
    }

    /**
     * @param $select
     * @return $this
     */
    public function select($select)
    {
        $this->select = $select;
        return $this;
    }

    private function getSelect()
    {
        return $this->select;
    }

    /**
     * @param $key
     * @param $value
     * @param null $like
     * @return $this
     *
     * @protectedVar where
     *
     * @by DB
     */
    public function where($key, $value = NULL, $scape = false)
    {
        if ($scape) {
            $this->where[] = array('method' => 'where', 'field' => $key, 'value' => $value);
        } else {
            $this->where[] = array('method' => 'where', 'field' => $key, 'value' => '"' . $value . '"');
        }
        return $this;
    }


    /**
     * @return string
     */
    private function getWhere($scape = true)
    {
        $where = '';
        if (count($this->where)) {
            $where = 'WHERE ';
        }
        foreach ($this->where as $field) {
            $mystring = $field['field'];
            $findme = ' ';
            $pos = strpos($mystring, $findme);
            if ($pos === false) {
                $comparison = ' = ';
            } else {
                $comparison = '';
            }
            if (isset($field['like'])) {
                $where .= $field['field'] . ' LIKE ' . "'%" . $field['value'] . "%'" . ' AND ';
            } else {
                $where .= $field['field'] . $comparison . $field['value'] . ' AND ';
            }

        }
        if (count($this->where)) {
            $where = substr($where, 0, -5);
        }
        return $where;
    }

    /**
     * @param $table
     * @param $on
     * @param string $tipo
     * @return $this
     */
    public function join($table, $on, $tipo = 'INNER')
    {
        $this->join[] = "$tipo JOIN $table ON $on ";
        return $this;
    }

    /**
     * @return string
     */
    private function getJoin()
    {
        return implode(' ', $this->join);
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     *
     * @by DB
     */
    public function order($key, $value)
    {
        $this->order = array('key' => $key, 'value' => $value);
        return $this;
    }

    /**
     * @return string
     */
    private function getOrder()
    {
        $order = '';
        if (count($this->order)) {
            $order = 'ORDER BY ' . $this->order['key'] . ' ' . $this->order['value'];
        }

        return $order;
    }

    /**
     * @param $start
     * @param $end
     * @return $this
     *
     * @by DB
     */
    public function limit($start, $end = false)
    {
        if ($end) {
            $this->limit = array('inicio' => $start, 'fim' => $end);
        } else {
            $this->limit = array('inicio' => $start);
        }

        return $this;
    }

    /**
     * @return string
     */
    private function getLimit()
    {
        $limit = '';
        if ((is_object($this->limit) || is_array($this->limit)) && count($this->limit)) {
            if (isset($this->limit['fim'])) {
                $limit = 'LIMIT ' . $this->limit['inicio'] . ', ' . $this->limit['fim'];
                return $limit;
            } else {
                $limit = isset($this->limit['inicio']) ? 'LIMIT ' . $this->limit['inicio'] : '';
                return $limit;
            }
        }
        return '';
    }

    /**
     * @param $field
     * @return $this
     */
    public function group_by($field)
    {
        $this->group_by = ' GROUP BY ' . $field;
        return $this;
    }

    /**
     * @return mixed
     */
    private function getGroupBy()
    {
        return $this->group_by;
    }

    /**
     * @param $field
     * @param $value
     * @return $this
     */
    public function order_by($field, $value)
    {
        $this->order_by = ' ORDER BY `' . $field . '` ' . $value;
        return $this;
    }

    /**
     * @return mixed
     */
    private function getOrderBy()
    {
        return $this->order_by;
    }

    /**
     * @param $operators
     * @return $this
     */
    public function having($operators)
    {
        $this->having[] = $operators;
        return $this;
    }

    /**
     * @return mixed|string
     */
    private function getHaving()
    {
        if (!count($this->having)) {
            return '';
        }
        $having = ' HAVING ' . implode(', ', $this->having);
        return $having;
    }

    public function get($table)
    {
        $where = $this->getWhere();
        $orderBy = $this->getOrderBy();
        $groupBy = $this->getGroupBy();
        $limit = $this->getLimit();
        $join = $this->getJoin();
        $having = $this->getHaving();
        $select = $this->getSelect() ? $this->getSelect() : '*';
        $this->options = array(
            'table' => $table,
            'select' => $select,
            'join' => $join,
            'where' => $where,
            'order_by' => $orderBy,
            'group_by' => $groupBy,
            'limit' => $limit,
            'having' => $having
        );
        $this->unsetOptions();
        return $this;
    }

    /**
     * @return array
     */
    public function result($showSql = false, $exit = true)
    {
        $sql = $this->getSql();
        if ($showSql) {
            if ($exit) {
                show_array($sql, true);
            } else {
                show_array($sql, false);
            }
        }
        $this->options = array();
        return $this->query($sql);
    }

    /**
     * @return object
     */
    public function row($showSql = false, $returnSql = false)
    {
        $this->limit(1);
        $sql = $this->getSql();
        $this->unsetOptions();
        $this->options = array();
        if ($showSql) {
            echo $sql;
            exit;
        }
        if ($returnSql) {
            return $sql;
        }
        return current($this->query($sql));
    }

    /**
     * @param string $tipo
     * @return array|string
     */
    public function getSql($tipo = 'SELECT')
    {
        $result = $this->getLI_Model()
            ->get($this->options['table'],
                $tipo . ' ' . $this->options['select'],
                $this->options['join'],
                $this->options['where'],
                $this->options['order_by'],
                $this->options['group_by'],
                $this->options['limit'],
                (isset($this->options['having']) ? $this->options['having'] : ''),
                true
            );
        $this->options = array();
        return $result;
    }

    /**
     *
     */
    private function unsetOptions()
    {
        $this->where = array();
        $this->order = '';
        $this->order_by = '';
        $this->join = array();
        $this->limit = '';
        $this->group_by = '';
        $this->select = false;
        $this->having = array();
    }

    public function resetDb()
    {
        $this->unsetOptions();
        return $this;
    }

    /**
     * @param $tabela
     * @param $dados
     * @param bool|false $params
     * @return int
     */
    public function insert($tabela, $dados, $params = false)
    {
        $campos = array();
        if ($params) {
            $campos = $dados;
        }
        if (!$params) {
            foreach ($dados as $key => $value) {
                $campos[$key] = $key;
                $params[$key] = $value;
            }
        }
        $lastInsertId = $this->getLI_Model()->insert($tabela, $campos, $params);
        $this->setLastInsertId($lastInsertId);
        return $this->getLastInsertId();
    }

    /**
     * @param $lastInsertId
     */
    private function setLastInsertId($lastInsertId)
    {
        $this->lastInsertId = $lastInsertId;
    }

    /**
     * @return int
     */
    public function getLastInsertId()
    {
        return (int)$this->lastInsertId;
    }


    /**
     * @param $tabela
     * @param $dados
     * @param $params
     * @return int
     * @by DB
     */
    public function update($tabela, $dados, $params = false)
    {
        $campos = array();
        if ($params) {
            $campos = $dados;
        }
        if (!$params) {
            foreach ($dados as $key => $value) {
                $campos[$key] = $key;
                $params[$key] = $value;
            }
        }

        $rowCount = $this->getLI_Model()->update($tabela, $campos, $params);
        return $rowCount;
    }

    /**
     * @param $table
     * @param bool $execute
     * @param bool $showSql
     * @return bool
     */
    public function delete($table, $execute = true, $showSql = false)
    {
        $where = $this->getWhere();
        $orderBy = $this->getOrderBy();
        $groupBy = $this->getGroupBy();
        $limit = $this->getLimit();
        $join = $this->getJoin();
        $select = " ";
        $this->options = array(
            'table' => $table,
            'select' => $select,
            'join' => $join,
            'where' => $where,
            'order_by' => $orderBy,
            'group_by' => $groupBy,
            'limit' => $limit
        );
        $this->unsetOptions();
        $sql = $this->getSql('DELETE');
        if ($execute) {
            $this->query($sql);
        }
        if ($showSql) {
            echo '<pre>';
            print_r($sql);
            echo '<br>';
            echo '</pre>';
        }
        return $execute;

    }


    public function query($sql)
    {
        return $this->getLI_Model()->getLI_Database()->selectDB($sql);
    }

    public function Capsule()
    {
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver' => LI_CONNECT_DBTYPE,
            'host' => LI_CONNECT_HOST,
            'database' => LI_CONNECT_DATABASE,
            'username' => LI_CONNECT_USER,
            'password' => LI_CONNECT_PASSWORD,
            'port' => LI_CONNECT_PORT,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
        return new \Core\DB\CapsuleGetter();
    }

}