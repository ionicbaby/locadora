<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 09:54
 */

namespace Core\Modelos;

use Aplicacao\Url;
use Core\Modelos\ModelagemDb;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuarios;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade;

class Carregamentos
{

    /**
     * @var \Core\Modelos\ModelagemDb
     */
    public $db;

    /**
     * @var Carregamentos
     */
    public $load;

    public function __construct()
    {
        $this->db = new ModelagemDb();
        $this->load = $this;
    }

    /**
     * @description, Verifica se é um módulo requisitando view, para que roteie ate o diretorio de views do proprio modulo
     * @return bool|string
     */
    private function eModulo()
    {
        $moduloInokeNamespace = '';
        foreach (debug_backtrace() as $nivel => $backTrace) {
            if ($nivel < 1 || $nivel > 2) {
                continue;
            }
            if (isset($backTrace['class'])) {
                if (substr($backTrace['class'], 0, 7) == 'Modulos') {
                    $moduloNamespace = explode('\\', $backTrace['class']);
                    foreach ($moduloNamespace as $key => $path) {
                        if ($key <= 2) {
                            $moduloInokeNamespace .= $path . '\\';
                        }
                    }
                }
            }
        }
        if ($moduloInokeNamespace != '') {
            return $moduloInokeNamespace;
        }
        return false;
    }

    /**
     * @param null $inicio
     * @param array $data
     * @param bool $handle
     * @return string
     */
    public function view($inicio = NULL, $data = array(), $handle = false, $emodulo = false)
    {
        if (!$emodulo) {
            $emodulo = $this->eModulo();
        }

        $pagina = '';
        extract($data);
        if (isset($data['pagina'])) {
            $pagina = $data['pagina'];
        }

        $defaultDir = LI_DIR_VIEWS;
        if (isset($data['moduloRouteLI'])) {
            $defaultDir = str_replace('\\', '/', 'src/' . $data['moduloRouteLI'] . 'Views/');;
        }
        $view = ($emodulo && !isset($data['pagina']) ? 'src/' . str_replace('\\', '/', $emodulo) . 'Views/' . $inicio : $defaultDir . $inicio) . '_view.php';

        if ($emodulo) {
            $moduloRouteLI = $emodulo;
        }
        if (!file_exists($view)) {
            die('<code>View: ' . $view . '; Não pode ser encontrada! </code>');
        }

        if ($handle) {
            ob_start();
            include $view;
            $handleView = ob_get_contents();
            ob_end_clean();
            return $handleView;
        }
        include $view;
    }


    /**
     * @param bool $gadgetName
     * @param bool $viewName
     * @param array $data
     */
    public function GadgetView($gadgetName = false, $viewName = false, $data = array())
    {
        if (is_array($viewName)) {
            echo '<code>Array given. Passe o nome da view para esse gadget</code>';
            return;
        }
        extract($data);
        if ($gadgetName) {
            if (!$viewName) {
                $viewName = 'index';
            }
            include LI_DIR_GADGETS . $gadgetName . '/views/' . $viewName . "_view.php";
        }
    }

}