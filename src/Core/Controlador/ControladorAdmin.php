<?php

/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 09:42
 */

namespace Core\Controlador;

use Aplicacao\Url;
use Core\Modelos\Carregamentos;
use Core\Modelos\ModelagemDb;

class ControladorAdmin
{

    /**
     * @var ModelagemDb
     */
    public $db;

    /**
     * @var Carregamentos
     */
    public $load;


    public function __construct()
    {
        if (!isset($_SESSION['admin'])) {
            $urlAtual = Url::Current();
            if ($urlAtual != Url::Base('Login')) {
                header('Location: ' . Url::Base('Login'));
            }
        }
        $this->setDB();
        $this->setLoad();
    }

    public function setDB()
    {
        $this->db = new ModelagemDb();
    }

    public function setLoad()
    {
        $this->load = new Carregamentos();
    }

    public function getTeste()
    {
        $std = new \stdClass();
        $std->meu_nome = 'Gabriel';
        $std->sobre_nome = 'Ariza';
        $primeiroStd = new \stdClass();
        $primeiroStd->desk = array('ind' => array('top' => $std));
        return array(
            0 => (new \stdClass()),
            32 => array(
                90 => array(
                    20 => $primeiroStd)));
    }

}