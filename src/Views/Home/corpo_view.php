<?php
/**
 * Created by PhpStorm.
 * User: Thiago
 * Date: 06/11/2016
 * Time: 12:19
 * @var $categorias array
 * @var $estados array
 */
\Aplicacao\Url::Apelido('CadastroLocadora/cadastrar', 'CadastroLocadora/cadastrar');

?>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Selecione uma data e um horário"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="col-md-12" style="padding: 0;">
                <div class="app-container" ng-app="dateTimeApp" ng-controller="dateTimeCtrl as ctrl" ng-cloak>
                    <div date-picker
                         datepicker-title="Selecione uma data"
                         picktime="true"
                         pickdate="true"
                         pickpast="false"
                         mondayfirst="false"
                         custom-message="Você selecionou"
                         selecteddate="ctrl.selected_date"
                         updatefn="ctrl.updateDate(newdate)">

                        <div class="datepicker"
                             ng-class="{
				'am': timeframe == 'am',
				'pm': timeframe == 'pm',
				'compact': compact
			}">
                            <div class="datepicker-header">
                                <div class="datepicker-title" ng-if="datepicker_title">{{ datepickerTitle }}</div>
                                <div class="datepicker-subheader">{{ customMessage }} {{ selectedDay }} {{
                                    monthNames[localdate.getMonth()] }} {{ localdate.getDate() }}, {{
                                    localdate.getFullYear() }}
                                </div>
                            </div>
                            <div class="datepicker-calendar">
                                <div class="calendar-header">
                                    <div class="goback" ng-click="moveBack()" ng-if="pickdate">
                                        <svg width="30" height="30">
                                            <path fill="none" stroke="#0DAD83" stroke-width="3" d="M19,6 l-9,9 l9,9"/>
                                        </svg>
                                    </div>
                                    <div class="current-month-container">{{ currentViewDate.getFullYear() }} {{
                                        currentMonthName() }}
                                    </div>
                                    <div class="goforward" ng-click="moveForward()" ng-if="pickdate">
                                        <svg width="30" height="30">
                                            <path fill="none" stroke="#0DAD83" stroke-width="3" d="M11,6 l9,9 l-9,9"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="calendar-day-header">
                                    <span ng-repeat="day in days" class="day-label">{{ day.short }}</span>
                                </div>
                                <div class="calendar-grid" ng-class="{false: 'no-hover'}[pickdate]">
                                    <div
                                            ng-class="{'no-hover': !day.showday}"
                                            ng-repeat="day in month"
                                            class="datecontainer"
                                            ng-style="{'margin-left': calcOffset(day, $index)}"
                                            track by $index>
                                        <div class="datenumber" ng-class="{'day-selected': day.selected }"
                                             ng-click="selectDate(day)">
                                            {{ day.daydate }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="timepicker" ng-if="picktime == 'true'">
                                <div ng-class="{'am': timeframe == 'am', 'pm': timeframe == 'pm' }">
                                    <div class="timepicker-container-outer" selectedtime="time" timetravel>
                                        <div class="timepicker-container-inner">
                                            <div class="timeline-container" ng-mousedown="timeSelectStart($event)"
                                                 sm-touchstart="timeSelectStart($event)">
                                                <div class="current-time">
                                                    <div class="actual-time">{{ time }}</div>
                                                </div>
                                                <div class="timeline">
                                                </div>
                                                <div class="hours-container">
                                                    <div class="hour-mark"
                                                         ng-repeat="hour in getHours() track by $index"></div>
                                                </div>
                                            </div>
                                            <div class="display-time">
                                                <div class="decrement-time" ng-click="adjustTime('decrease')">
                                                    <svg width="24" height="24">
                                                        <path stroke="white" stroke-width="2" d="M8,12 h8"/>
                                                    </svg>
                                                </div>
                                                <div class="time" ng-class="{'time-active': edittime.active}">
                                                    <input type="text" class="time-input" ng-model="edittime.input"
                                                           ng-keydown="changeInputTime($event)"
                                                           ng-focus="edittime.active = true; edittime.digits = [];"
                                                           ng-blur="edittime.active = false"/>
                                                    <div class="formatted-time">{{ edittime.formatted }}</div>
                                                </div>
                                                <div class="increment-time" ng-click="adjustTime('increase')">
                                                    <svg width="24" height="24">
                                                        <path stroke="white" stroke-width="2" d="M12,7 v10 M7,12 h10"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="am-pm-container">
                                                <div class="am-pm-button" ng-click="changetime('am');">Manhã</div>
                                                <div class="am-pm-button" ng-click="changetime('pm');">Tarde</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="buttons-container">
                                <div class="cancel-button" onclick="$('.modal').modal('hide');">Cancelar</div>
                                <div class="save-button" onclick="$('.modal').modal('hide');">salvar</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid banner-mobile d-sm-none">
</div>
<div class="container-fluid banner d-none d-sm-block">
</div>

<section title="Alugue seu carro ideal" class="container mensagemPrincipal">
    <div class="row" style="margin-bottom: 50px">
        <div class="col-md titulo-section theme-title-one">
            <div>
                <h1>Alugue seu carro ideal</h1>
                <p style="font-size: 14px">
                    Oferecemos uma rede de locadoras independentes espalhadas por todo Brasil. São locadoras de veículos
                    credenciadas e com anos de atividade no ramo. Basta nos enviar o seu destino que em minutos receberá
                    orçamentos das melhores locadoras. Aproveite as vantanges de flexibilidade em locação que só
                    locadoras indepententes e alternativas podem oferecer.
                </p>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md">
            <div class=" sombra-1">
                <!-- começa formulário -->
                <form id="pedir_orcamento" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="insiraNome">Escolha o Estado</label>
                                <select name="estados" class="form-control" required>
                                    <option value="-1">Selecione o estado de destino</option>
                                    <?php
                                    foreach ($estados as $estado) {
                                        ?>
                                        <option value="<?php echo $estado->sigla_uf; ?>"><?php echo $estado->titulo; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="insiraNome">Escolha a Cidade</label>
                                <select disabled="disabled" name="cidades" class="form-control" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 25px">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="col-md-3" style="padding: 0;">
                                                <button type="button" class="btn btn-secondary"
                                                        onclick="$('.modal').modal();campo = 'data_horiario_retirada';">
                                                    Data e Hora da Retirada
                                                </button>
                                            </div>
                                            <div class="col-md-9">
                                                <div id="data_horiario_retirada_view"
                                                     style="margin-left: 30%; padding: 5px; font-weight: bold; color: royalblue;"></div>
                                                <input type="text" value="<?php echo date('d/m/Y H:m:s'); ?>" hidden
                                                       name="data_horiario_retirada">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group date">
                                            <div class="input-group">
                                                <div class="col-md-3" style="padding: 0;">
                                                    <button type="button" class="btn btn-secondary"
                                                            onclick="$('.modal').modal(); campo = 'data_horiario_entrega';">
                                                        Data e Hora da Entrega
                                                    </button>
                                                </div>
                                                <div class="col-md-9">
                                                    <div id="data_horiario_entrega_view"
                                                         style="margin-left: 30%; padding: 5px; font-weight: bold; color: royalblue;"></div>
                                                    <input type="text" hidden name="data_horiario_entrega">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIM DA LOCALIZAÇÃO-->

                    <!-- CARD CATEGORIAS -->
                    <div class="row" style="margin-top: 25px">
                        <div class="col-md-6" style="margin-bottom: 25px;">
                            <div class="col-md-12" style="padding: 0;">
                                <div class="form-group col-md-12" style="padding: 0;">
                                    <label for="insiraTel">Categoria do Veículo </label>
                                    <select class="custom-select form-control" id="categorias_veiculo" name="categoria_carro">
                                        <?php
                                        foreach ($categorias as $categoria) {
                                            ?>
                                            <option <?php echo $categoria->id == 1 ? 'selected' : ''; ?>
                                                    data-imagem="<?php echo \Aplicacao\Url::Base() . $categoria->imagem; ?>"
                                                    value="<?php echo $categoria->titulo; ?>"><?php echo $categoria->titulo; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12" id="area_de_escolha_das_categorias"></div>
                            </div>
                            <div class="col-md-12" id="card_categoria" style="padding: 0">
                                <div class="col-md-12 card-body" id="dados_da_categoria" style="display: none;">
                                    <div class="card" id="conteudo_categoria">
                                        <img class="card-img-top" id="imagem_da_categoria" src=""
                                             alt="Card image cap">
                                        <div class="card-body" id="descricao_da_categoria">
                                            <h5 class="card-title" id="titulo_categoria"></h5>
                                            <p class="card-text" id="atributos_da_categoria"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- DADOS PESSOAIS -->


                        <div class="col-md-6" style="margin-top: 25px">

                            <div style="height: auto; background: linear-gradient(to left, #005c97, #363795); color: white; padding: 3px">
                                <h4 class="text-center"
                                    style="font-family: Roboto; margin-top: 10px; letter-spacing: 1px">OPCIONAIS</h4>
                            </div>
                            <div class="row" style="margin-top: 25px; margin-bottom: 25px; color: #015b97">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6  custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="customRadioInline1" name="opcionais[]"
                                                       value="Navegador GPS" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline1">Navegador
                                                    GPS</label>
                                            </div>

                                            <div class="col-md-6  custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="customRadioInline2" name="opcionais[]"
                                                       value="Cadeirinha" class="custom-control-input">
                                                <label class="custom-control-label"
                                                       for="customRadioInline2">Cadeirinha</label>
                                            </div>
                                            <div class="col-md-6  custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="customRadioInline5" name="opcionais[]"
                                                       value="Bebê Conforto" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline5">Bebê
                                                    Conforto</label>
                                            </div>
                                            <div class="col-md-6  custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="customRadioInline3" name="opcionais[]"
                                                       value="Conduto Adicional" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline3">Condutor
                                                    Adicional</label>
                                            </div>

                                            <div class="col-md-6  custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="customRadioInline4" name="opcionais[]"
                                                       value="Assento de Elevação" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadioInline4">Assento de
                                                    Elevação</label>
                                            </div>

                                            <div class="col-md-6  custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="customRadioInline6" name="opcionais[]"
                                                       value="Wi-Fi" class="custom-control-input">
                                                <label class="custom-control-label"
                                                       for="customRadioInline6">Wi-Fi</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-secondary text-center" role="alert">
                                <b>DADOS PESSOAIS</b>
                            </div>
                            <div class="form-group">
                                <label for="dataInicial">Nome Completo</label>
                                <div class="input-group">
                                    <input type="text" name="nome_completo" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dataInicial">Email</label>
                                <div class="input-group">
                                    <input type="email" name="email" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="dataInicial">DDD</label>
                                        <div class="input-group">
                                            <input type="text" name="ddd" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="dataInicial">Telefone</label>
                                        <div class="input-group">
                                            <input type="text" name="telefone" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="insiraTel">Operador</label>
                                        <select class="custom-select" name="operadora">
                                            <option value='Oi'>Oi</option>
                                            <option value='Tim'>Tim</option>
                                            <option value='Claro'>Claro</option>
                                            <option value='Vivo'>Vivo</option>
                                            <option value='Nextel'>Nextel</option>
                                            <option value='Fixo'>Fixo</option>
                                            <option value='Outras'>Outra</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Contato -->
                            <div class="form-group">
                                <label for="insiraTel">Como quer receber o contato?</label>
                                <select class="custom-select" name="tipo_contato">
                                    <option value='email'>pelo email</option>
                                    <option value='telefone'>por telefone</option>
                                    <option value='whatsapp'>por whatsapp</option>
                                    <option value='email, telefone ou whatsapp'>por email, telefone ou whatsapp
                                    </option>
                                </select>
                            </div>
                            <!-- FIM DO CONTATO -->
                            <div class="col-md-12" style="padding: 0; margin-top: 15px">
                                <button type="submit" class="btn btn-success" id="submitForm">Pedir meu carro</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- Termina formulário -->
            </div>
        </div>
    </div>
</section>

<section title="Como funciona" class="fluid-container" style="background-color: #fafafa; height: auto; margin-top: 50px; padding: 25px;">
    <div class="container">
        <div class="row" style="margin-bottom: 25px">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h1 title="Como funciona" class="text-center">Como Funciona</h1>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row como_funciona">
            <section class="col-md-4" style="margin-top:10px;margin-bottom:10px;">Lorem ipsum dolor sit amet, consectetur
                adipisicing elit. Amet enim esse fuga itaque non quaerat tenetur voluptatum? Ab accusamus adipisci
                deserunt fugit nulla reprehenderit tempora. Aliquid hic in quos reprehenderit.
            </section>
            <section class="col-md-4" style="margin-top:10px;margin-bottom:10px;">Lorem ipsum dolor sit amet, consectetur
                adipisicing elit. A aperiam asperiores assumenda blanditiis dolor, dolores earum enim explicabo itaque
                molestiae natus nemo odit praesentium quia quis quisquam quod! Voluptate, voluptatum.
            </section>
            <section class="col-md-4" style="margin-top:10px;margin-bottom:10px;">Lorem ipsum dolor sit amet, consectetur
                adipisicing elit. Animi blanditiis commodi ducimus earum incidunt, iusto neque omnis quibusdam quidem
                ratione rem, repellendus sapiente similique? Assumenda blanditiis doloremque est possimus sunt.
            </section>
        </div>
    </div>

</section>
<section title="Vantanges de usar AlugueCarro.com" class="container-fluid" style="padding: 25px;">
    <div class="container" style="margin-top: 50px; margin-bottom: 50px;">
        <div class="row" style="margin-bottom: 25px">
            <div class="col-md-12" style="margin-bottom: 25px">
                <h1 title="Vantanges de usar AlugueCarro.com" class="text-center">Vantanges de usar AlugueCarro.com</h1>
            </div>
        </div>
        <div class="row como_funciona">
            <section class="col-md-4">
                <h2><b>Comodidade</b></h2>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet enim esse fuga itaque non quaerat tenetur
                voluptatum? Ab accusamus adipisci deserunt fugit nulla reprehenderit tempora. Aliquid hic in quos
                reprehenderit.
            </section>
            <section class="col-md-4">
                <h2><b>Qualidade</b></h2>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam asperiores assumenda blanditiis
                dolor, dolores earum enim explicabo itaque molestiae natus nemo odit praesentium quia quis quisquam
                quod! Voluptate, voluptatum.
            </section>
            <section class="col-md-4">
                <h2><b>Segurança</b></h2>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi blanditiis commodi ducimus earum
                incidunt, iusto neque omnis quibusdam quidem ratione rem, repellendus sapiente similique? Assumenda
                blanditiis doloremque est possimus sunt.
            </section>
        </div>
    </div>
</section>

