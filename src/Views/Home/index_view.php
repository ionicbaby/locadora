<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 10:00
 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php $this->load->view('Home/head', $data); ?>
</head>
<body>
<?php
if (isset($pagina)) {
    unset($data['pagina']);
    if (isset($moduloRouteLI)) {
        $data['moduloRouteLI'] = $moduloRouteLI;
    }
    if (!isset($desabilitarHeader) || $desabilitarHeader = false) {
        $this->load->view('Home/Header', array());
    }
    $this->load->view($pagina, $data);
    if (!isset($desabilitarFooter) || $desabilitarFooter = false) {
        $this->load->view('Home/Footer', array());
    }
}
if (!isset($pagina)) {
    echo 'Nenhuma view foi inicializada';
}
include 'src/Assets/Js/phphelper.php';
?>
</body>
</html>