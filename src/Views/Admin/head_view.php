<?php

?>
<title><?php echo 'locadora';echo isset($titulo) && $titulo ? ' - '.$titulo : '';?></title>
<meta charset="UTF-8">
<!-- <meta name="viewport" content="width=device-width,initial-scale=1"> -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Gabriel Ariza Gomes de Castro">
<meta name="robots" CONTENT="INDEX, FOLLOW">
<meta name="copyright" CONTENT="Gabriel Ariza Gomes de Castro">
<meta name="copyright" CONTENT="Gabriel Ariza Gomes de Castro">
<meta name="keywords" content="<?php echo isset($palavras_chaves) ? $palavras_chaves : '';?>">

<meta name="description" content="<?php echo $descricao = isset($descricao) && $descricao ? $descricao : 'Sobre';?>">
<link rel="manifest" href="<?php echo \Aplicacao\Url::Base('manifest.json');?>">
<link rel="canonical" content="<?php echo \Aplicacao\Url::Current();?>">

<meta property="og:image" content="<?php echo isset($ogimage) && $ogimage ? $ogimage : \Aplicacao\Url::Base('/Assets/Images/Default/sem_imagem.jpg');?>">
<meta property="og:image:type" content="jpg">
<meta property="og:image:width" content="800">
<meta property="og:image:height" content="600">

<meta property="og:locale" content="pt_BR"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="<?php echo isset($ogtitle) && $ogtitle ? $ogtitle : 'Locadora de veiculos';?>"/>
<meta property="og:description" content="<?php echo isset($ogdescription) && $ogdescription ? $ogdescription : $descricao;?>"/>
<meta property="og:url" content="<?php echo \Aplicacao\Url::Current();?>"/>
<meta property="article:section" content="<?php echo isset($ogsection) && $ogsection ? $ogsection : 'Post';?>"/>

<link rel="icon" type="text/css" href="<?php echo \Aplicacao\Url::Base('Assets/Images/Default/favicon.png');?>"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<link href='https://fonts.googleapis.com/css?family=Passion+One:400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,700,500italic,500,400italic,300italic,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,400i,500,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:100,300,400,400i,500,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:400,500,600,700" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- PROPER -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- PROPER -->


<!-- BOOTSTRAP -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="<?php \Aplicacao\Ferramentas\GetAssets::Plugins('Bootstrap/datepicker.min.css'); ?>" rel='stylesheet' type='text/css'>
<script src="<?php echo \Aplicacao\Ferramentas\GetAssets::Plugins('Bootstrap/datepicker.min.js');?>"></script>
<script src="<?php echo \Aplicacao\Ferramentas\GetAssets::Plugins('Bootstrap/datepicker.pt-BR.min.js');?>"></script>

<!-- BOOTSTRAP -->
<!-- CSS DEFAULT -->
<?php \Aplicacao\Ferramentas\GetAssets::CSS('default');?>
<!-- CSS DEFAULT -->

<!-- JS DEFAULT -->
<?php \Aplicacao\Ferramentas\GetAssets::JS('default');?>
<!-- JS DEFAULT -->

<!-- date time picker -->
<link href="<?php \Aplicacao\Ferramentas\GetAssets::Plugins('DateTimePicker/angular-material.min.css'); ?>" rel='stylesheet' type='text/css'>
<link href="<?php \Aplicacao\Ferramentas\GetAssets::Plugins('DateTimePicker/default.css'); ?>" rel='stylesheet' type='text/css'>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.2/angular.min.js"></script>
<script src="<?php echo \Aplicacao\Ferramentas\GetAssets::Plugins('DateTimePicker/default.js');?>"></script>

<!-- date time picker -->

