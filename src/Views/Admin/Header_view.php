<?php
$dd = date("w");

switch($dd) {

    case"0": $dia_semana = "Domingo"; break;

    case"1": $dia_semana = "Segunda-feira"; break;

    case"2": $dia_semana = "Terça-feira"; break;

    case"3": $dia_semana = "Quarta-feira"; break;

    case"4": $dia_semana = "Quinta-feira"; break;

    case"5": $dia_semana = "Sexta-feira"; break;

    case"6": $dia_semana = "Sábado"; break;

}

$hoje = date( 'd-m-Y');

?>

<div class="container-fluid barra-menu">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <a class="navbar-brand" href="#"><b>ADMIN ALUGUE UM CARRO</b></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo \Aplicacao\Url::Base('endPoints/logout');?>">Logout</a>
                    </li>
                </ul>
            </div>
            <div class="float-right alert alert-success">

            </div>
        </nav>
    </div>
</div>

