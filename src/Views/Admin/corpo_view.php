<?php





;?>

<div class="fluid-container">
    <div class="container" style="margin-top: 50px; margin-bottom: 50px">
        <div class="row">
            <div class="col-md-3">
                <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                    <div class="card-header">Total Emails Enviados</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <i class="fas fa-envelope" style="font-size: 2rem; color: #fff"></i>
                            </div>
                            <div class="col-md-8 text-center">
                                <?php

                                    echo '<span style="font-size: 24px">'.$total.'</span>';

                                ;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                    <div class="card-header">Total Cadastro Locadoras</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <i class="far fa-address-book" style="font-size: 2rem; color: #fff"></i>
                            </div>
                            <div class="col-md-8 text-center">
                                <?php

                                echo '<span style="font-size: 24px">'.$locadoras.'</span>';

                                ;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                    <div class="card-header">Total Clientes Atendidos</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <i class="fas fa-user-plus" style="font-size: 2rem; color: #fff"></i>
                            </div>
                            <div class="col-md-8 text-center">
                                <?php

                                echo '<span style="font-size: 24px">'.$total_clientes.'</span>';

                                ;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                    <div class="card-header">Total Acessos</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <i class="fas fa-tachometer-alt" style="font-size: 2rem; color: #fff"></i>
                            </div>
                            <div class="col-md-8">
                                Plugin Analytics
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="fluid-container" style="background-color: #fff  ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php

                    foreach ($last_week as $Key => $info){
                        echo $info->nome .'Total de emails: '. $info->total . '<br>';
                    }

                ;?>
            </div>
        </div>
    </div>
</div>



