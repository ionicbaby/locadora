<?php
/**
 * Created by PhpStorm.
 * User: Shared
 * Date: 30/04/2018
 * Time: 18:12
 */
?>

<div class="fluid-container" style="margin-top: 50px; margin-bottom: 50px">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-4">
                <div class="card" style="width: ">
                    <img class="card-img-top" src="https://cdn.dribbble.com/users/5976/screenshots/1978947/ssg_security_padlock_locker_lock_logo_design_symbol_by_alex_tass.jpg" alt="Card image cap">
                    <div class="card-body">
                        <form id="login" method="get">
                            <div style="margin-top: 15px; margin-bottom: 15px">
                                <input type="email" class="form-control" name="usuario" placeholder="Usuário">
                            </div>
                            <div style="margin-top: 15px; margin-bottom: 15px">
                                <input type="password" class="form-control" name="senha" placeholder="Senha">
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary" id="logarButton" style="width: 100%">Logar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
