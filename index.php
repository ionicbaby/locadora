<?php
/*
 * @author Gabriel
 * @description inicializar projeto
 * @date 05/11/2016 09:26:48
 */
//if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
//    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//    header('HTTP/1.1 301 Moved Permanently');
//    header('Location: ' . $redirect);
//    exit();
//}
ini_set( 'session.cookie_httponly', 1 );
date_default_timezone_set('America/Sao_Paulo');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, OPTIONS, PUT');

require_once 'Config.php';
require_once 'vendor/autoload.php';
require_once 'system/helpers/FuncoesHelper.php';
session_start();
\Aplicacao\Url::Apelido('CadastroLocadora/cadastrar', 'CadastroLocadora/cadastrar');
\Aplicacao\Url::Apelido('Admin', 'admin');
\Aplicacao\Url::Apelido('Admin/RelatoriosEmailsEnviados', 'admin/relatorios/emails-enviados');
\Aplicacao\Url::Apelido('Admin/RelatoriosCadastrosLocadoras', 'admin/relatorios/cadastros-de-locadoras');

$route = new \Aplicacao\RotearCamadas();
