<!doctype html>
<html lang="pt-BR">
<body>
<!--CATEGORIA ON-->
<div class="card">
    <div class="card-title" >
        <p class="titulo-categoria">Categoria <span>On</span></p>
    </div>
    <img class="card-img-top" src="https://prdmovida.blob.core.windows.net/public/imagens/cars/t3_grupo_AX.jpg" alt="Categoria On">
    <div class="card-body">
        <div class="row text-center ">
            <div class="col-md" style="font-size: 16px">
                <span class="badge badge-primary">Econômico/1.0 </span>
                <span class="badge badge-primary">4 Portas </span>
                <span class="badge badge-primary">AC </span>
                <span class="badge badge-primary">DH/Vid </span>
                <span class="badge badge-primary">Trav. Elét. </span>
                <span class="badge badge-primary">CD ou USB </span>
                <span class="badge badge-primary">5 PAX </span>
                <span class="badge badge-primary">2 Malas </span>
                <span class="badge badge-success">Freios ABS </span>
                <span class="badge badge-success">Air Bag </span>
            </div>
        </div>
    </div>
</div>
<!--FIM CATEGORIA ON-->
<!-- ECONÔMICO COM AR -->
<div class="card">
    <div class="card-title" >
        <p class="titulo-categoria">Categoria <span>On</span></p>
    </div>
    <img class="card-img-top" src="https://prdmovida.blob.core.windows.net/public/imagens/cars/t3_grupo_AX.jpg" alt="Categoria On">
    <div class="card-body">
        <div class="row text-center ">
            <div class="col-md" style="font-size: 16px">
                <span class="badge badge-primary">Econômico/1.0 </span>
                <span class="badge badge-primary">4 Portas </span>
                <span class="badge badge-primary">AC </span>
                <span class="badge badge-primary">DH/Vid </span>
                <span class="badge badge-primary">Trav. Elét. </span>
                <span class="badge badge-primary">CD ou USB </span>
                <span class="badge badge-primary">5 PAX </span>
                <span class="badge badge-primary">2 Malas </span>
                <span class="badge badge-success">Freios ABS </span>
                <span class="badge badge-success">Air Bag </span>
            </div>
        </div>
    </div>
</div>
<!--FIM ECONÔMICO COM AR-->
</body>
</html>